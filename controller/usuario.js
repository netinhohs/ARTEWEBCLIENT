var usuario = {
    login: function(email, senha) {
        $.ajax({
            url: "https://www.doocati.com.br/tcc/webservice/login/" + email + "/" + senha,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                console.log(data);
                if (data == false) {
                    $("#txtErro").html("Usuario não encontrado! Digite novamente email e senha.");
                    $("#msgErro").show();
                } else {
                    setUsuarioLogado(data.usu_id);
                    window.location.href = 'index.php';
                }
            },
            error: function(data) {
                console.log(data);
            }
        });
    },
    google: function(obj) {
        var id = obj.usu_id_google;
        $.ajax({
            url: "https://www.doocati.com.br/tcc/webservice/loginGoogle/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                console.log(data);
                if (data.usu_id > 0) {
                    setUsuarioLogado(data.usu_id);
                    window.location.href = 'index.php';
                } else {
                    $.ajax({
                        url: "https://www.doocati.com.br/tcc/webservice/usuario/google",
                        type: "POST",
                        dataType: "JSON",
                        data: JSON.stringify(obj),
                        success: function(data) {
                            console.log(data);
                            if (data.id > 0) {
                                setUsuarioLogado(data.id);
                                window.location.href = 'index.php';
                            } else {
                                $("#txtErro").html("Ocorreu um erro favor entrar em contato com o suporte!");
                                $("#msgErro").show();
                            }
                        },
                        error: function(data) {
                            console.log(data);
                            $("#txtErro").html("Ocorreu um erro favor entrar em contato com o suporte!");
                            $("#msgErro").show();
                        }
                    });
                }
            },
            error: function(data) {
                console.log(data);
                $("#txtErro").html("Ocorreu um erro favor entrar em contato com o suporte!");
                $("#msgErro").show();
            }
        });
    },
    detalhar: function(id) {
        $.ajax({
            url: "https://www.doocati.com.br/tcc/webservice/usuario/detalhar/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                console.log(data);
                if (data.usu_id > 0) {
                    $("#usu_nome").val(data.usu_nome);
                    $("#usu_email").val(data.usu_email);
                    $("#usu_cpf").val(data.usu_cpf);
                    $("#usu_telefone").val(data.usu_telefone);
                    $("#usu_descricao").val(data.usu_descricao);
                    $("#usu_celular").val(data.usu_celular);
                    $("#usu_cadastro_completo").val(data.usu_cadastro_completo);                                      
                    if (Number(data.obras)>0) {
                        $("#obras").append(data.obras + "<small>Minhas obras</small>");
                    } else {
                        $("#obras").append(0 + "<small>Minhas obras</small>");
                    }
                } else {
                    $("#txtErro").html("Usuario não encontrado! Digite novamente email e senha.");
                    $("#msgErro").show();
                }
            },
            error: function(data) {
                console.log(data);
            }
        });
    },
    menuPerfil: function(id) {
        $.ajax({
            url: "https://www.doocati.com.br/tcc/webservice/usuario/detalhar/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                console.log(data);
                if (data.usu_id > 0) {
                    if (data.usu_imagem !== "") {
                        $("#dspImg").attr("src",data.usu_imagem);
                    } else {
                        $("#dspImg").attr("src", "images/user.jpg");
                    }
                    $("#dspNome").html(data.usu_nome);

                    if (data.usu_cadastro_completo == false) {
                        $("#dspMsgCadastro").html("<span class='required'>Lembre - se de finalizar o cadastro para poder cadastrar as obras!</span>");
                        $("#dspMsgCadastro").show();
                        $("#dspSexo").show();
                        $("#dspCpf").show();
                    }
                } 
            },
            error: function(data) {
                console.log(data);
            }
        });
    },
    atualizarPerfil: function(id) {
        var obj = $("#form1").serializeObject();
        console.log(obj);
        $.ajax({
            url: "https://www.doocati.com.br/tcc/webservice/usuario/atualizarPerfil/" + id,
            type: "PUT",
            dataType: "JSON",
            data: JSON.stringify(obj),
            success: function(data) {
                console.log(data);
                if (data.status == true) {

                    if (obj.usu_senha != "") {
                        $.ajax({
                            url: "https://www.doocati.com.br/tcc/webservice/usuario/alterarSenha/" + id,
                            type: "PUT",
                            dataType: "JSON",
                            data: JSON.stringify(obj.usu_senha),
                            success: function(data) {
                                console.log(data);
                                window.location.href = 'perfil.php?sucess=ok';
                            },
                            error: function(data) {
                                console.log(data);
                            }
                        });
                    }
                    window.location.href = 'perfil.php?sucess=ok';
                } else {
                    $("#txtErro").html("Ocorreu um erro favor entrar em contato com o suporte!");
                    $("#msgErro").show();
                }
            },
            error: function(data) {
                console.log(data);
                $("#txtErro").html("Ocorreu um erro favor entrar em contato com o suporte!");
                $("#msgErro").show();
            }
        });
    },
    registroRapido: function() {
        var serialize = $("#form1").serializeObject();
        console.log(serialize);
        $.ajax({
            url: "https://www.doocati.com.br/tcc/webservice/usuario/registroRapido/",
            type: "POST",
            dataType: "JSON",
            data: JSON.stringify(serialize),
            success: function(data) {
                console.log(data);
                if (data.id > 0) {
                    window.location.href = 'login.php?create=ok';
                }
            },
            error: function(data) {
                console.log(data);
                $("#txtErro").html("Ocorreu um erro favor entrar em contato com o suporte!");
                $("#msgErro").show();
            }
        });
    },
    inativarConta: function(id){
        $.ajax({
            url: "https://www.doocati.com.br/tcc/webservice/desativarArtistasPedentes/"+id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
               console.log(data);
            },
            error: function(data) {
                console.log(data);
            }
        });
    }
}