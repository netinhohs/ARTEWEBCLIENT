var obra = {
  buscarCepCorreios:function(cep){
        $.ajax({
            url:"https://viacep.com.br/ws/"+cep+"/json",
            type:"GET",
            dataType:"JSON",
            success:function(data){
                $("#ate_endereco").val(data.logradouro);
                $("#ate_bairro").val(data.bairro);
                $("#ate_cidade").val(data.localidade);
                $("#ate_estado").val(data.uf);
                console.log(data);
            },
            error:function(data){
                console.log(data);
            }
        });	
	},
    inserir:function(id){
        geocoder = new google.maps.Geocoder();
        var titulo,descricao,categoria,usuario,endereco,cep,bairro,estado,numero,complemento,cidade,latitude,longitude;
        var arr = {};
        titulo = $("#obr_titulo").val();
        descricao = $("#obr_descricao").val();
        categoria = $("#cat_obra_id").val();
        usuario = id;
     
        var serialize = { obr_titulo: titulo, obr_descricao: descricao, cat_obra_id: categoria, usu_id: usuario };
        for(var i = 0; i < document.querySelectorAll("#fotos").length; i++){
            arr[i] = document.querySelector("#fotos")[i].value;
        }
                
        $.ajax({
        url:"https://www.doocati.com.br/tcc/webservice/obra/",
        type:"POST",
        dataType:"JSON",
        data: JSON.stringify(serialize),
        success:function(data){
            console.log(data);
            if(data.id > 0){
                
                $.ajax({
                    url:"https://www.doocati.com.br/tcc/webservice/obra/imagem/"+data.id,
                    type:"POST",
                    dataType:"JSON",
                    data: JSON.stringify(arr),                    
                    success:function(data){
                        console.log(data);
                    },
                    error:function(data){
                        console.log(data);
                    }
                });
                window.location = "ok.php";
            }
        },
        error:function(data){
            console.log(data);
            $("#txtErro").html("Ocorreu um erro favor entrar em contato com o suporte!");
            $("#msgErro").show();
        }
        });            

    },
    listarTodos(){
        $.ajax({
            url:"https://www.doocati.com.br/tcc/webservice/obra/imagem/",
            type:"GET",
            dataType:"JSON",
            success:function(data){
                console.log(data);
                $("#showObras").html("");
                for(var i = 0; i < data.length; i++){
                    $("#showObras").append('<div class="ad-item row">'
                                            +'<div class="item-image-box col-sm-4">'
                                                +'<div class="item-image">'
                                                    +'<a href="details.html"><img src="'+data[i].img_url+'" alt="Image" class="img-responsive"></a>'
                                                +'</div>'
                                            +'</div>'
                                            +'<div class="item-info col-sm-8">'
                                                +'<div class="ad-info">'
                                                //   +'<h3 class="item-price">$800.00</h3>'
                                                    +'<h4 class="item-title"><a href="detalhar_obra.php?obra_id='+data[i].obr_id+'">'+data[i].obr_titulo+'</a></h4>'
                                                    +'<div class="item-cat">'
                                                        +'<span><a href="#">'+data[i].obr_descricao+'</a></span>'
                                                    +'</div>'
                                                    +'<div class="item-cat">'
                                                        +'<span><a href="#">'+data[i].cat_obra_descricao+'</a></span>'
                                                    +'</div>'									
                                                +'</div>'
                                                +'<div class="ad-meta">'
                                                    +'<div class="meta-content">'
                                                        +'<span class="dated"><a href="#">'+ new Date() + '</a></span>'
                                                    //   +'<a href="#" class="tag"><i class="fa fa-tags"></i> New</a>'
                                                    +'</div>'										
                                                    +'<div class="user-option pull-right">'
                                                    +'<a href="#" data-toggle="tooltip" data-placement="top" title="'+data[i].ate_cidade+', '+data[i].ate_estado+'"><i class="fa fa-map-marker"></i> </a>'
                                                    //    +'<a class="online" href="#" data-toggle="tooltip" data-placement="top" title="Detalhar Artista"><i class="fa fa-user"></i> </a>'											
                                                    +'</div>'
                                                +'</div>'
                                            +'</div>'
                                        +'</div>');
                                        }
            },
            error:function(data){
                console.log(data);
            }
        });	
    },
    listarById(id){
         $.ajax({
            url:"https://www.doocati.com.br/tcc/webservice/usuario/obra/imagem/"+id,
            type:"GET",
            dataType:"JSON",
            success:function(data){
                console.log(data);
                $("#mostrarMinhas").html("");
                for(var i = 0; i < data.length; i++){
                    $("#mostrarMinhas").append('<div class="ad-item row">'
								+'<div class="item-image-box col-sm-4">'
									+'<div class="item-image">'
										+'<a href="javascript:;"><img src="'+data[i].img_url+'" alt="Image" class="img-responsive"></a>'
									+'</div>'
								+'</div>'
								+'<div class="item-info col-sm-8">'
								+'<div class="ad-info">'
									//	+'<h3 class="item-price">$800.00</h3>'
										+'<h4 class="item-title"><a href="javascript:;">'+data[i].obr_titulo+'</a></h4>'
										+'<div class="item-cat">'
											+'<span><a href="#">'+data[i].obr_descricao+'</a></span>'
										+'</div>'
                                        +'<div class="item-cat">'
											+'<span><a href="#">'+data[i].cat_obra_descricao+'</a></span>'
										+'</div>'										
									+'</div>'
									+'<div class="ad-meta">'
									+'<div class="meta-content">'
											+'<span class="dated">Ranking: <a href="javascript:;">  ★★★★ </a></span>'
											+'<span class="visitors">Visitas: 221</span>' 
										+'</div>'										
										+'<div class="user-option pull-right">'
											+'<a class="edit-item" onclick="alterarObra('+data[i].obr_id+');" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Editar obra"><i class="fa fa-pencil"></i></a>'
											+'<a class="delete-item" onclick="excluirObra('+data[i].obr_id+');" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Deletar obra"><i class="fa fa-times"></i></a>'
										+'</div>'
									+'</div>'
								+'</div>'
							+'</div>');
                                        }
            },
            error:function(data){
                console.log(data);
            }
        });	
    },
    excluir:function(id){
      $.ajax({
         url:"https://www.doocati.com.br/tcc/webservice/obra/teste/"+id,
         type:"DELETE",
         dataType:"JSON",
         success:function(data){
            if (data.status == true){
               bootbox.alert("Item excluido com sucesso!", function() {
                   window.location.reload();
               });
            }
            console.log(data);
         },
         error:function(data){
           bootbox.alert(data);
            console.log(data);
         }
      });
   },
   listIndex(){
        $.ajax({
            url:"https://www.doocati.com.br/tcc/webservice/obra/imagem/",
            type:"GET",
            dataType:"JSON",
            success:function(data){
                console.log(data);
                $("#showIndex").html("");
                for(var i = 0; i < data.length; i++){
                    $("#showIndex").append('<div class="item-image-box col-sm-4">'
						+'<div class="item-image">'
							+'<a href="details.html"><img src="'+data[i].img_url+'" alt="Image" class="img-responsive"></a>'
							+'<a href="#" class="verified" data-toggle="tooltip" data-placement="left" title="Verified"><i class="fa fa-check-square-o"></i></a>'
						+'</div>'
					+'</div>'
					+'<div class="item-info col-sm-8">'			
						+'<div class="ad-info">'
							//+'<h3 class="item-price">500</h3>'
							+'<h4 class="item-title"><a href="detalhar_obra.php?obra_id='+data[i].obr_id+'">'+data[i].obr_titulo+'</a></h4>'
							+'<div class="item-cat">'
								  +'<span><a href="#">'+data[i].obr_descricao+'</a></span>'
							+'</div>'
                            +'<div class="item-cat">'
								  +'<span><a href="#">'+data[i].cat_obra_descricao+'</a></span>'
							+'</div>'
						+'</div>'
						+'<div class="ad-meta">'
							+'<div class="meta-content">'
								+'<span class="dated"><a href="#">7 Jan, 16  10:10 pm </a></span>'
								+'<a href="#" class="tag"><i class="fa fa-tags"></i> Used</a>'
							+'</div>'									
							+'<div class="user-option pull-right">'
								+'<a href="#" data-toggle="tooltip" data-placement="top" title="Los Angeles, USA"><i class="fa fa-map-marker"></i> </a>'
								+'<a class="online" href="#" data-toggle="tooltip" data-placement="top" title="Dealer"><i class="fa fa-suitcase"></i> </a>'											
							+'</div>'
						+'</div>'
					+'</div>'
                    +'<div class="featured-top">'
                        +'&nbsp;'
                    +'</div>');
                    if(i == 5){
                        break;
                    }
                }
            },
            error:function(data){
                console.log(data);
            }
        });
   },
   filtroCategoria(id){
       $.ajax({
            url:"https://www.doocati.com.br/tcc/webservice/filtro/categoria/"+id,
            type:"GET",
            dataType:"JSON",
            success:function(data){
                console.log(data);
                $("#showObras").html("");
                if(data.length == 0){
                    $("#showObras").append('<div class="row"><div class="text-center"><h4>Nenhum Resultado em contrado!</h4></div></div>');
                }else{
                    $("#cate_"+id).css("color","#00a651"); //background-color:#00a651;
                    $("#cate_"+id).css("outline","none");
                    $("#cate_"+id).css("text-decoration","none");
                }
                for(var i = 0; i < data.length; i++){
                    $("#showObras").append('<div class="ad-item row">'
                    +'<div class="item-image-box col-sm-4">'
                        +'<div class="item-image">'
                            +'<a href="details.html"><img src="'+data[i].img_url+'" alt="Image" class="img-responsive"></a>'
                        +'</div>'
                    +'</div>'
                    +'<div class="item-info col-sm-8">'
                        +'<div class="ad-info">'
                        //   +'<h3 class="item-price">$800.00</h3>'
                            +'<h4 class="item-title"><a href="detalhar_obra.php?obra_id='+data[i].obr_id+'">'+data[i].obr_titulo+'</a></h4>'
                            +'<div class="item-cat">'
                                +'<span><a href="#">'+data[i].obr_descricao+'</a></span>'
                            +'</div>'
                            +'<div class="item-cat">'
                                +'<span><a href="#">'+data[i].cat_obra_descricao+'</a></span>'
                            +'</div>'									
                        +'</div>'
                        +'<div class="ad-meta">'
                            +'<div class="meta-content">'
                                +'<span class="dated"><a href="#">'+ new Date() + '</a></span>'
                            //   +'<a href="#" class="tag"><i class="fa fa-tags"></i> New</a>'
                            +'</div>'										
                            +'<div class="user-option pull-right">'
                            +'<a href="#" data-toggle="tooltip" data-placement="top" title="'+data[i].ate_cidade+', '+data[i].ate_estado+'"><i class="fa fa-map-marker"></i> </a>'
                            //    +'<a class="online" href="#" data-toggle="tooltip" data-placement="top" title="Detalhar Artista"><i class="fa fa-user"></i> </a>'											
                            +'</div>'
                        +'</div>'
                    +'</div>'
                +'</div>');
                }
            },
            error:function(data){
                console.log(data);
            }
        });	
   },
   filtroLike(id){
       $.ajax({
            url:"https://www.doocati.com.br/tcc/webservice/filtro/"+id,
            type:"GET",
            dataType:"JSON",
            success:function(data){
                console.log(data);
                $("#showObras").html("");
                if(data.length == 0){
                    $("#showObras").append('<div class="row"><div class="text-center"><h4>Nenhum Resultado em contrado!</h4></div></div>');
                }
                for(var i = 0; i < data.length; i++){
                    $("#showObras").append('<div class="ad-item row">'
                    +'<div class="item-image-box col-sm-4">'
                        +'<div class="item-image">'
                            +'<a href="details.html"><img src="'+data[i].img_url+'" alt="Image" class="img-responsive"></a>'
                        +'</div>'
                    +'</div>'
                    +'<div class="item-info col-sm-8">'
                        +'<div class="ad-info">'
                        //   +'<h3 class="item-price">$800.00</h3>'
                            +'<h4 class="item-title"><a href="detalhar_obra.php?obra_id='+data[i].obr_id+'">'+data[i].obr_titulo+'</a></h4>'
                            +'<div class="item-cat">'
                                +'<span><a href="#">'+data[i].obr_descricao+'</a></span>'
                            +'</div>'
                            +'<div class="item-cat">'
                                +'<span><a href="#">'+data[i].cat_obra_descricao+'</a></span>'
                            +'</div>'									
                        +'</div>'
                        +'<div class="ad-meta">'
                            +'<div class="meta-content">'
                                +'<span class="dated"><a href="#">'+ new Date() + '</a></span>'
                            //   +'<a href="#" class="tag"><i class="fa fa-tags"></i> New</a>'
                            +'</div>'										
                            +'<div class="user-option pull-right">'
                            +'<a href="#" data-toggle="tooltip" data-placement="top" title="'+data[i].ate_cidade+', '+data[i].ate_estado+'"><i class="fa fa-map-marker"></i> </a>'
                            //    +'<a class="online" href="#" data-toggle="tooltip" data-placement="top" title="Detalhar Artista"><i class="fa fa-user"></i> </a>'											
                            +'</div>'
                        +'</div>'
                    +'</div>'
                +'</div>');
                }
            },
            error:function(data){
                console.log(data);
            }
        });	
   },
    detalhar:function(id){
        $.ajax({
            url:"https://www.doocati.com.br/tcc/webservice/obra/detalhar/"+id,
            type:"GET",
            dataType:"JSON",
            success:function(data){
                console.log(data);
                $("#txtNome").html(data.obr_titulo);
                $("#obr_categoria").html(data.cat_obra_descricao);
                $("#usu_nome").html(data.usu_nome);
                $("#usu_email").html(data.usu_email);
                $("#usu_telefone").html(data.usu_telefone+" / "+data.usu_celular);dspDescricao
                $("#dspDescricao").html(data.obr_descricao);

                for(var i = 0; i < data.imagens.length; i++){
                    var imgClass = "";
                    if(i == 0){
                        imgClass = "active";
                    }
                    $("#ImgGrande").append(
                    '<li data-target="#product-carousel" data-slide-to="0" class="'+imgClass+'">'
                        +'<img src="'+data.imagens[i].img_url+'" alt="'+data.obr_titulo+'" class="img-responsive">'
                    +'</li>'); 

                    $("#ImgPequena").append(
                    '<div class="item '+imgClass+'">'
                        +'<div class="carousel-image">'
                            +'<img src="'+data.imagens[i].img_url+'" alt="Featured Image" class="img-responsive" style="height: 500px; width: auto; min-width: 617px;">'
                        +'</div>'
                    +'</div>');
                };
            },
            error:function(data){
                console.log(data);
            }
        });	
	}
}