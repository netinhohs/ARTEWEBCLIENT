var atelie = {
  buscarCepCorreios:function(cep){
        $.ajax({
            url:"https://viacep.com.br/ws/"+cep+"/json",
            type:"GET",
            dataType:"JSON",
            success:function(data){
                $("#ate_endereco").val(data.logradouro);
                $("#ate_bairro").val(data.bairro);
                $("#ate_cidade").val(data.localidade);
                $("#ate_estado").val(data.uf);
                console.log(data);
            },
            error:function(data){
                console.log(data);
            }
        });	
	},
    inserir:function(id){
        geocoder = new google.maps.Geocoder();
        var titulo,descricao,categoria,usuario,endereco,cep,bairro,estado,numero,complemento,cidade,latitude,longitude;
        usuario = id;
        nome  = $("#ate_nome").val();
        endereco = $("#ate_endereco").val();
        numero = $("#ate_numero").val();
        complemento = $("#ate_complemento").val();
        cep = $("#ate_cep").val();
        bairro = $("#ate_bairro").val();
        cidade = $("#ate_cidade").val();
        estado = $("#ate_estado").val();
        address = numero+","+endereco+","+bairro+","+cidade+","+estado;
     
          geocoder.geocode( { 'address': address}, function(results, status) {
              if (status == google.maps.GeocoderStatus.OK) {
                latitude = results[0].geometry.location.lat();
                longitude = results[0].geometry.location.lng();

                var atelie = {ate_nome: nome, ate_endereco: endereco, ate_cep: cep, ate_numero: numero, ate_complemento: complemento, ate_bairro: bairro, ate_estado: estado, ate_cidade: cidade,ate_latitude:latitude,ate_longitude:longitude,usu_id: usuario};
                
               $.ajax({
                url:"https://www.doocati.com.br/tcc/webservice/atelie/"+usuario,
                type:"POST",
                dataType:"JSON",
                data: JSON.stringify(atelie),
                success:function(data){
                    if(data.id > 0){
                        bootbox.alert("Ateliê cadastrado com sucesso!", function() {
                            window.location.href = 'meu_atelie.php';
                        });
                    }
                },
                error:function(data){
                    console.log(data);
                    $("#txtErro").html("Ocorreu um erro favor entrar em contato com o suporte!");
                    $("#msgErro").show();
                }
                });            
              } 
            }); 

    },
    listarTodos(){
        $.ajax({
            url:"https://www.doocati.com.br/tcc/webservice/obra/imagem/",
            type:"GET",
            dataType:"JSON",
            success:function(data){
                console.log(data);
                $("#showObras").html("");
                for(var i = 0; i < data.length; i++){
                    $("#showObras").append('<div class="ad-item row">'
                                            +'<div class="item-image-box col-sm-4">'
                                                +'<div class="item-image">'
                                                    +'<a href="details.html"><img src="img/'+data[i].img_url+'" alt="Image" class="img-responsive"></a>'
                                                +'</div>'
                                            +'</div>'
                                            +'<div class="item-info col-sm-8">'
                                                +'<div class="ad-info">'
                                                //   +'<h3 class="item-price">$800.00</h3>'
                                                    +'<h4 class="item-title"><a href="detalhar_obra.php?obra_id='+data[i].obr_id+'">'+data[i].obr_titulo+'</a></h4>'
                                                    +'<div class="item-cat">'
                                                        +'<span><a href="#">'+data[i].obr_descricao+'</a></span>'
                                                    +'</div>'
                                                    +'<div class="item-cat">'
                                                        +'<span><a href="#">'+data[i].cat_obra_descricao+'</a></span>'
                                                    +'</div>'									
                                                +'</div>'
                                                +'<div class="ad-meta">'
                                                    +'<div class="meta-content">'
                                                        +'<span class="dated"><a href="#">'+ new Date() + '</a></span>'
                                                    //   +'<a href="#" class="tag"><i class="fa fa-tags"></i> New</a>'
                                                    +'</div>'										
                                                    +'<div class="user-option pull-right">'
                                                    +'<a href="#" data-toggle="tooltip" data-placement="top" title="'+data[i].ate_cidade+', '+data[i].ate_estado+'"><i class="fa fa-map-marker"></i> </a>'
                                                    //    +'<a class="online" href="#" data-toggle="tooltip" data-placement="top" title="Detalhar Artista"><i class="fa fa-user"></i> </a>'											
                                                    +'</div>'
                                                +'</div>'
                                            +'</div>'
                                        +'</div>');
                                        }
            },
            error:function(data){
                console.log(data);
            }
        });	
    },
    listarById(id){
         $.ajax({
            url:"https://www.doocati.com.br/tcc/webservice/artistaatelie/"+id,
            type:"GET",
            dataType:"JSON",
            success:function(data){
                console.log(data);
                $("#mostrarMinhas").html("");
                for(var i = 0; i < data.length; i++){
                    $("#mostrarMinhas").append('<div class="ad-item col-sm-5" style="padding-left: 0; padding-right: 0; margin-left: 40px;">'
								+'<div class="item-info">'
								+'<div class="ad-info">'
									//	+'<h3 class="item-price">$800.00</h3>'
										+'<h4 class="item-title"><a href="javascript:;">'+data[i].ate_nome+'</a></h4>'
										+'<div class="item-cat">'
											+'<span><a href="#">'+data[i].ate_endereco+' Nº '+data[i].ate_numero+'</a></span>'
										+'</div>'
                                        +'<div class="item-cat">'
											+'<span><a href="#">'+data[i].ate_cep+', '+data[i].ate_cidade+' - '+data[i].ate_estado+'</a></span>'
										+'</div>'										
									+'</div>'
									+'<div class="ad-meta">'									
										+'<div class="user-option pull-right">'
											+'<a class="edit-item" href="cadastrar_meu_atelie.php?ate_id='+data[i].ate_id+'" data-toggle="tooltip" data-placement="top" title="Editar Ateliê"><i class="fa fa-pencil"></i></a>'
											+'<a class="delete-item" onclick="excluirAtelie('+data[i].ate_id+');" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Deletar Ateliê"><i class="fa fa-times"></i></a>'
										+'</div>'
									+'</div>'
								+'</div>'
							+'</div>');
                                        }
            },
            error:function(data){
                console.log(data);
            }
        });	
    },
    excluir:function(id){
      $.ajax({
         url:"https://www.doocati.com.br/tcc/webservice/atelie/teste/"+id,
         type:"DELETE",
         dataType:"JSON",
         success:function(data){
            if (data.status == true){
               bootbox.alert("Item excluido com sucesso!", function() {
                   window.location.reload();
               });
            }
            console.log(data);
         },
         error:function(data){
           bootbox.alert(data);
            console.log(data);
         }
      });
   },
   listIndex(){
        $.ajax({
            url:"https://www.doocati.com.br/tcc/webservice/obra/imagem/",
            type:"GET",
            dataType:"JSON",
            success:function(data){
                console.log(data);
                $("#showIndex").html("");
                for(var i = 0; i < data.length; i++){
                    $("#showIndex").append('<div class="item-image-box col-sm-4">'
						+'<div class="item-image">'
							+'<a href="details.html"><img src="img/'+data[i].img_url+'" alt="Image" class="img-responsive"></a>'
							+'<a href="#" class="verified" data-toggle="tooltip" data-placement="left" title="Verified"><i class="fa fa-check-square-o"></i></a>'
						+'</div>'
					+'</div>'
					+'<div class="item-info col-sm-8">'			
						+'<div class="ad-info">'
							//+'<h3 class="item-price">500</h3>'
							+'<h4 class="item-title"><a href="detalhar_obra.php?obra_id='+data[i].obr_id+'">'+data[i].obr_titulo+'</a></h4>'
							+'<div class="item-cat">'
								  +'<span><a href="#">'+data[i].obr_descricao+'</a></span>'
							+'</div>'
                            +'<div class="item-cat">'
								  +'<span><a href="#">'+data[i].cat_obra_descricao+'</a></span>'
							+'</div>'
						+'</div>'
						+'<div class="ad-meta">'
							+'<div class="meta-content">'
								+'<span class="dated"><a href="#">7 Jan, 16  10:10 pm </a></span>'
								+'<a href="#" class="tag"><i class="fa fa-tags"></i> Used</a>'
							+'</div>'									
							+'<div class="user-option pull-right">'
								+'<a href="#" data-toggle="tooltip" data-placement="top" title="Los Angeles, USA"><i class="fa fa-map-marker"></i> </a>'
								+'<a class="online" href="#" data-toggle="tooltip" data-placement="top" title="Dealer"><i class="fa fa-suitcase"></i> </a>'											
							+'</div>'
						+'</div>'
					+'</div>'
                    +'<div class="featured-top">'
                        +'&nbsp;'
                    +'</div>');
                    if(i == 5){
                        break;
                    }
                }
            },
            error:function(data){
                console.log(data);
            }
        });
   },
   detalhar:function(id){
		$.ajax({
         url:"https://www.doocati.com.br/tcc/webservice/atelie/detalhar/"+id,
         type:"GET",
         dataType:"JSON",
         success:function(data){
             $("#ate_id").val(data.ate_id);
			 $("#ate_nome").val(data.ate_nome);
			 $("#ate_cep").val(data.ate_cep);
             $("#ate_endereco").val(data.ate_endereco);
             $("#ate_numero").val(data.ate_numero);
             $("#ate_complemento").val(data.ate_complemento);
             $("#ate_bairro").val(data.ate_bairro);
             $("#ate_cidade").val(data.ate_cidade);
             $("#ate_estado").val(data.ate_estado);
            console.log(data);
         },
         error:function(data){
            console.log(data);
         }
      });
	},
   alterar:function(id){
		var serialize = $("#form1").serializeObject();
		$.ajax({
         url:"https://www.doocati.com.br/tcc/webservice/tipoobra/"+id,
         type:"PUT",
         dataType:"JSON",
		 data: JSON.stringify(serialize),
         success:function(data){
            if (data.status == true){
               $("#txtSucesso").html("Alteração executada com sucesso!");
               $("#msgSucesso").show();
            }
            console.log(data);
         },
         error:function(data){
            $("#txtErro").html("Ocorreu algum erro na alteração, entrar em contato com o suporte!");
            $("#msgErro").show();
         }
      });
	},
   /*filtroCategoria(id){
       $.ajax({
            url:"https://www.doocati.com.br/tcc/webservice/filtro/categoria/"+id,
            type:"GET",
            dataType:"JSON",
            success:function(data){
                console.log(data);
                $("#showObras").html("");
                if(data.length == 0){
                    $("#showObras").append('<div class="row"><div class="text-center"><h4>Nenhum Resultado em contrado!</h4></div></div>');
                }else{
                    $("#cate_"+id).css("color","#00a651"); //background-color:#00a651;
                    $("#cate_"+id).css("outline","none");
                    $("#cate_"+id).css("text-decoration","none");
                }
                for(var i = 0; i < data.length; i++){
                    $("#showObras").append('<div class="ad-item row">'
                    +'<div class="item-image-box col-sm-4">'
                        +'<div class="item-image">'
                            +'<a href="details.html"><img src="img/'+data[i].img_url+'" alt="Image" class="img-responsive"></a>'
                        +'</div>'
                    +'</div>'
                    +'<div class="item-info col-sm-8">'
                        +'<div class="ad-info">'
                        //   +'<h3 class="item-price">$800.00</h3>'
                            +'<h4 class="item-title"><a href="detalhar_obra.php?obra_id='+data[i].obr_id+'">'+data[i].obr_titulo+'</a></h4>'
                            +'<div class="item-cat">'
                                +'<span><a href="#">'+data[i].obr_descricao+'</a></span>'
                            +'</div>'
                            +'<div class="item-cat">'
                                +'<span><a href="#">'+data[i].cat_obra_descricao+'</a></span>'
                            +'</div>'									
                        +'</div>'
                        +'<div class="ad-meta">'
                            +'<div class="meta-content">'
                                +'<span class="dated"><a href="#">'+ new Date() + '</a></span>'
                            //   +'<a href="#" class="tag"><i class="fa fa-tags"></i> New</a>'
                            +'</div>'										
                            +'<div class="user-option pull-right">'
                            +'<a href="#" data-toggle="tooltip" data-placement="top" title="'+data[i].ate_cidade+', '+data[i].ate_estado+'"><i class="fa fa-map-marker"></i> </a>'
                            //    +'<a class="online" href="#" data-toggle="tooltip" data-placement="top" title="Detalhar Artista"><i class="fa fa-user"></i> </a>'											
                            +'</div>'
                        +'</div>'
                    +'</div>'
                +'</div>');
                }
            },
            error:function(data){
                console.log(data);
            }
        });	
   },
   filtroLike(id){
       $.ajax({
            url:"https://www.doocati.com.br/tcc/webservice/filtro/"+id,
            type:"GET",
            dataType:"JSON",
            success:function(data){
                console.log(data);
                $("#showObras").html("");
                if(data.length == 0){
                    $("#showObras").append('<div class="row"><div class="text-center"><h4>Nenhum Resultado em contrado!</h4></div></div>');
                }
                for(var i = 0; i < data.length; i++){
                    $("#showObras").append('<div class="ad-item row">'
                    +'<div class="item-image-box col-sm-4">'
                        +'<div class="item-image">'
                            +'<a href="details.html"><img src="img/'+data[i].img_url+'" alt="Image" class="img-responsive"></a>'
                        +'</div>'
                    +'</div>'
                    +'<div class="item-info col-sm-8">'
                        +'<div class="ad-info">'
                        //   +'<h3 class="item-price">$800.00</h3>'
                            +'<h4 class="item-title"><a href="detalhar_obra.php?obra_id='+data[i].obr_id+'">'+data[i].obr_titulo+'</a></h4>'
                            +'<div class="item-cat">'
                                +'<span><a href="#">'+data[i].obr_descricao+'</a></span>'
                            +'</div>'
                            +'<div class="item-cat">'
                                +'<span><a href="#">'+data[i].cat_obra_descricao+'</a></span>'
                            +'</div>'									
                        +'</div>'
                        +'<div class="ad-meta">'
                            +'<div class="meta-content">'
                                +'<span class="dated"><a href="#">'+ new Date() + '</a></span>'
                            //   +'<a href="#" class="tag"><i class="fa fa-tags"></i> New</a>'
                            +'</div>'										
                            +'<div class="user-option pull-right">'
                            +'<a href="#" data-toggle="tooltip" data-placement="top" title="'+data[i].ate_cidade+', '+data[i].ate_estado+'"><i class="fa fa-map-marker"></i> </a>'
                            //    +'<a class="online" href="#" data-toggle="tooltip" data-placement="top" title="Detalhar Artista"><i class="fa fa-user"></i> </a>'											
                            +'</div>'
                        +'</div>'
                    +'</div>'
                +'</div>');
                }
            },
            error:function(data){
                console.log(data);
            }
        });	
   }*/
}