var avaliacao = {
   listById:function(id){
        $.ajax({
            url: "https://www.doocati.com.br/tcc/webservice/avaliacoes/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                console.log(data);
                $("#showAvaliacoes").html("");
                for(var i = 0; i < data.length; i++){
                    $("#showAvaliacoes").append('<div class="form-group" style="margin-bottom: 5px;">'
                        +'<label for="ava_descricao" style="float: left;">'+data[i].usu_nome+'</label>'
                        +'<div id="nota'+data[i].ava_id+'" class="rateit" data-rateit-value="'+data[i].ava_nota+'" data-rateit-ispreset="true" data-rateit-readonly="true" style="float: left; padding-left: 10px;"></div>'
                        +'<textarea class="form-control" readonly id="ava_descricao" name="ava_descricao" rows="2">'+data[i].ava_descricao+'</textarea>'
                    +'</div>');
                    $("#nota"+data[i].ava_id).rateit();
                } 
            },
            error: function(data) {
                console.log(data);
            }
        }); 
   },
   inserir:function(){
        var dados = {"ava_titulo" : "", "ava_descricao" : $("#ava_descricao").val(),
                     "ava_nota" : $("input[name='ava_nota']:checked").val(), "ava_ativo" : "1",
                     "usu_id_artista" : $("#artista_id").val(), "usu_id" : getUsuarioLogado() };
        $.ajax({
            url: "https://www.doocati.com.br/tcc/webservice/avaliacoes/",
            type: "POST",
            dataType: "JSON",
            data : JSON.stringify(dados),
            success: function(data) {
                console.log(data);
                if(data.id > 0){
                    bootbox.confirm("Avaliação realizada com sucesso!", function(result){ 
                        if (result == true){
                            window.location.href = 'detalhar_artista.php?artista_id='+$("#artista_id").val();
                        } 
                    });
                }
            },
            error: function(data) {
                console.log(data);
            }
        });    
    },
    rank:function(id){
        $.ajax({
            url: "https://www.doocati.com.br/tcc/webservice/avaliacoes/rank/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                console.log(data);
                if(data.qtd > 0){
                    var total = (data.nota / data.qtd);
                    $("#dspRank").html("");
                    $("#dspRank").append('<div id="teste1" class="rateit" data-rateit-value="'+total+'" data-rateit-ispreset="true" data-rateit-readonly="true"></div>');
                    $('#teste1').rateit();
                }
            },
            error: function(data) {
                console.log(data);
            }
        });
    }
}