var artista = {
   detalharArtista:function(id){
         $.ajax({
            url: "https://www.doocati.com.br/tcc/webservice/usuario/detalhar/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                console.log(data);
                if (data.usu_id > 0) {
                    $("#txtNome").html(data.usu_nome);
                    $("#usu_email").html(data.usu_email);
                    $("#usu_nascimento").html(data.usu_data_nascimento);
                    $("#txtDescricao").html(data.usu_descricao);
                    if(data.usu_genero == 'M'){
                        $("#usu_sexo").html("Masculino");
                    }else{
                        $("#usu_sexo").html("Feminino");
                    }
                    img = '<img src="'+data.usu_imagem+'" alt="Image" class="img-responsive" style="width: 600px; height: 400px;">';
                    $("#img_artista").html(img);
                    $("#txtTells").html(data.usu_telefone+' / '+data.usu_celular);                                      
                    $("#txtObra").html(data.obras);
                  
                } else {
                    $("#txtErro").html("Usuario não encontrado! Digite novamente email e senha.");
                    $("#msgErro").show();
                }
            },
            error: function(data) {
                console.log(data);
            }
        }); 
   },
   listIndex:function(){
        $.ajax({
            url:"https://www.doocati.com.br/tcc/webservice/artista/all",
            type:"GET",
            dataType:"JSON",
            success:function(data){
                console.log(data);
                $("#showIndex").html("");
                for(var i = 0; i < data.length; i++){
                    var img = "";
                    img = '<a href="detalhar_artista.php?artista_id='+data[i].usu_id+'"><img src="'+data[i].usu_imagem+'" alt="Image" class="img-responsive"></a>';
                    
                    $("#showIndex").append('<div class="item-image-box col-sm-2">'
						+'<div class="item-image">'
							+img
							+'<a href="#" class="verified" data-toggle="tooltip" data-placement="left" title="Verified"><i class="fa fa-check-square-o"></i></a>'
						+'</div>'
					+'</div>'
					+'<div class="item-info col-sm-10">'			
						+'<div class="ad-info">'
							//+'<h3 class="item-price">500</h3>'
							+'<h4 class="item-title"><a href="detalhar_artista.php?artista_id='+data[i].usu_id+'">'+data[i].usu_nome+'</a></h4>'
							+'<div class="item-cat">'
								  +'<span><a href="#">'+data[i].usu_email+'</a></span>'
							+'</div>'
                            +'<div class="item-cat">'
								  +'<span><a href="#">'+data[i].usu_descricao+'</a></span>'
							+'</div>'
						+'</div>'
						/*+'<div class="ad-meta">'
							+'<div class="meta-content">'
								+'<span class="dated"><a href="#">7 Jan, 16  10:10 pm </a></span>'
								+'<a href="#" class="tag"><i class="fa fa-tags"></i> Used</a>'
							+'</div>'									
							+'<div class="user-option pull-right">'
								+'<a href="#" data-toggle="tooltip" data-placement="top" title="Los Angeles, USA"><i class="fa fa-map-marker"></i> </a>'
								+'<a class="online" href="#" data-toggle="tooltip" data-placement="top" title="Dealer"><i class="fa fa-suitcase"></i> </a>'											
							+'</div>'
						+'</div>'*/
					+'</div>'
                    +'<div class="featured-top">'
                        +'&nbsp;'
                    +'</div>');
                    if(i == 5){
                        break;
                    }
                }
            },
            error:function(data){
                console.log(data);
            }
        });
   },
   listarObraById:function(id){
         $.ajax({
            url:"https://www.doocati.com.br/tcc/webservice/usuario/obra/imagem/"+id,
            type:"GET",
            dataType:"JSON",
            success:function(data){
                console.log(data);
                $("#minhasObras").html("");
                for(var i = 0; i < data.length; i++){
                    $("#minhasObras").append('<div class="ad-item row">'
                        +'<div class="item-image-box col-sm-4">'
                            +'<div class="item-image">'
                                +'<a href="javascript:;"><img src="'+data[i].img_url+'" alt="Image" class="img-responsive"></a>'
                            +'</div>'
                        +'</div>'
                        +'<div class="item-info col-sm-8">'
                        +'<div class="ad-info">'
                            //	+'<h3 class="item-price">$800.00</h3>'
                                +'<h4 class="item-title"><a href="detalhar_obra.php?id_obra='+data[i].obr_id+'">'+data[i].obr_titulo+'</a></h4>'
                                +'<div class="item-cat">'
                                    +'<span><a href="#">'+data[i].obr_descricao+'</a></span>'
                                +'</div>'
                                +'<div class="item-cat">'
                                    +'<span><a href="#">'+data[i].cat_obra_descricao+'</a></span>'
                                +'</div>'										
                            +'</div>'
                            +'<div class="ad-meta">'
                            +'<div class="meta-content">'
                                //    +'<span class="dated">Ranking: <a href="javascript:;">  ★★★★ </a></span>'
                                    +'<span class="visitors">Visitas: 221</span>' 
                                +'</div>'										
                                //+'<div class="user-option pull-right">'
                                 //   +'<a class="edit-item" onclick="alterarObra('+data[i].obr_id+');" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Editar obra"><i class="fa fa-pencil"></i></a>'
                                 //   +'<a class="delete-item" onclick="excluirObra('+data[i].obr_id+');" href="javascript:;" data-toggle="tooltip" data-placement="top" title="Deletar obra"><i class="fa fa-times"></i></a>'
                                //+'</div>'
                            +'</div>'
                        +'</div>'
                    +'</div>');
                }
            },
            error:function(data){
                console.log(data);
            }
        });	
    },
   filtroCategoria:function(id){
       $.ajax({
            url:"https://www.doocati.com.br/tcc/webservice/filtro/categoria/"+id,
            type:"GET",
            dataType:"JSON",
            success:function(data){
                console.log(data);
                $("#showObras").html("");
                if(data.length == 0){
                    $("#showObras").append('<div class="row"><div class="text-center"><h4>Nenhum Resultado em contrado!</h4></div></div>');
                }else{
                    $("#cate_"+id).css("color","#00a651"); //background-color:#00a651;
                    $("#cate_"+id).css("outline","none");
                    $("#cate_"+id).css("text-decoration","none");
                }
                for(var i = 0; i < data.length; i++){
                     var img = "";
                    img = '<a href="detalhar_artista.php?artista_id='+data[i].usu_id+'"><img src="'+data[i].usu_imagem+'" alt="Image" class="img-responsive"></a>';

                    $("#showObras").append('<div class="ad-item row">'
                    +'<div class="item-image-box col-sm-4">'
                        +'<div class="item-image">'
							+img
                        +'</div>'
                    +'</div>'
                    +'<div class="item-info col-sm-8">'
                        +'<div class="ad-info">'
                        //   +'<h3 class="item-price">$800.00</h3>'
                           +'<h4 class="item-title"><a href="detalhar_artista.php?artista_id='+data[i].usu_id+'">'+data[i].usu_nome+'</a></h4>'
                           +'<div class="item-cat">'
                                +'<span><a href="#">'+data[i].usu_email+'</a></span>'
                            +'</div>'
                            +'<div class="item-cat">'
                                +'<span><a href="#">'+data[i].usu_descricao+'</a></span>'
                            +'</div>'									
                        +'</div>'
                       /* +'<div class="ad-meta">'
                            +'<div class="meta-content">'
                                +'<span class="dated"><a href="#">'+ new Date() + '</a></span>'
                            +'</div>'										
                            +'<div class="user-option pull-right">'
                            +'<a href="#" data-toggle="tooltip" data-placement="top"><i class="fa fa-map-marker"></i> </a>'											
                            +'</div>'
                        +'</div>'*/
                    +'</div>'
                +'</div>');
                }
            },
            error:function(data){
                console.log(data);
            }
        });	
   },
   filtroLike:function(id){
       $.ajax({
            url:"https://www.doocati.com.br/tcc/webservice/filtro/"+id,
            type:"GET",
            dataType:"JSON",
            success:function(data){
                console.log(data);
                $("#showObras").html("");
                if(data.length == 0){
                    $("#showObras").append('<div class="row"><div class="text-center"><h4>Nenhum Resultado encontrado!</h4></div></div>');
                }
                for(var i = 0; i < data.length; i++){
                     var img = "";
                     img = '<a href="detalhar_artista.php?artista_id='+data[i].usu_id+'"><img src="'+data[i].usu_imagem+'" alt="Image" class="img-responsive"></a>';
                  
                    $("#showObras").append('<div class="ad-item row">'
                    +'<div class="item-image-box col-sm-4">'
                        +'<div class="item-image">'
                            +img
                        +'</div>'
                    +'</div>'
                    +'<div class="item-info col-sm-8">'
                        +'<div class="ad-info">'
                        //   +'<h3 class="item-price">$800.00</h3>'
                             +'<h4 class="item-title"><a href="detalhar_artista.php?artista_id='+data[i].usu_id+'">'+data[i].usu_nome+'</a></h4>'
                            +'<div class="item-cat">'
                                +'<span><a href="#">'+data[i].usu_email+'</a></span>'
                            +'</div>'
                            +'<div class="item-cat">'
                                +'<span><a href="#">'+data[i].usu_descricao+'</a></span>'
                            +'</div>'										
                        +'</div>'
                     /*   +'<div class="ad-meta">'
                            +'<div class="meta-content">'
                                +'<span class="dated"><a href="#">'+ new Date() + '</a></span>'
                            +'</div>'										
                            +'<div class="user-option pull-right">'
                            +'<a href="#" data-toggle="tooltip" data-placement="top"><i class="fa fa-map-marker"></i> </a>'											
                            +'</div>'
                        +'</div>'*/
                    +'</div>'
                +'</div>');
                }
            },
            error:function(data){
                console.log(data);
            }
        });	
   },
   listarTodos:function(){
        $.ajax({
            url:"https://www.doocati.com.br/tcc/webservice/artista/all",
            type:"GET",
            dataType:"JSON",
            success:function(data){
                console.log(data);
                $("#showObras").html("");
                for(var i = 0; i < data.length; i++){
                    var img = "";
                    img = '<a href="detalhar_artista.php?artista_id='+data[i].usu_id+'"><img src="'+data[i].usu_imagem+'" alt="Image" class="img-responsive"></a>';
  
                    $("#showObras").append('<div class="ad-item row">'
                    +'<div class="item-image-box col-sm-4">'
                        +'<div class="item-image">'
                            +img
                        +'</div>'
                    +'</div>'
                    +'<div class="item-info col-sm-8">'
                        +'<div class="ad-info">'
                            +'<h4 class="item-title"><a href="detalhar_artista.php?artista_id='+data[i].usu_id+'">'+data[i].usu_nome+'</a></h4>'
                            +'<div class="item-cat">'
                                +'<span><a href="#">'+data[i].usu_email+'</a></span>'
                            +'</div>'
                            +'<div class="item-cat">'
                                +'<span><a href="#">'+data[i].usu_descricao+'</a></span>'
                            +'</div>'									
                        +'</div>'
                        /*+'<div class="ad-meta">'
                            +'<div class="meta-content">'
                                +'<span class="dated"><a href="#">'+ new Date() + '</a></span>'
                            +'</div>'										
                            +'<div class="user-option pull-right">'
                            +'<a href="#" data-toggle="tooltip" data-placement="top"><i class="fa fa-map-marker"></i> </a>'											
                            +'</div>'
                        +'</div>'*/
                    +'</div>'
                +'</div>');
                }
            },
            error:function(data){
                console.log(data);
            }
        });	
    }
}