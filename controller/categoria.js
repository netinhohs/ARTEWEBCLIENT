var categoria = {
  listar:function(){
      $.ajax({
         url:"https://www.doocati.com.br/tcc/webservice/tipoobra",
         type:"GET",
         dataType:"JSON",
         success:function(data){
            console.log(data);
            var cont = 3;
            for(var i = 0; i < data.length; i++){
               $("#listCategorias").append('<li>'+
                     ' <a href="#cat'+cont+'" aria-controls="cat'+cont+'" onclick="CateogiaAdd('+cont+','+data[i].cat_obra_id+',\''+data[i].cat_obra_descricao+'\')" role="tab" data-toggle="tab">'+
                        '<span class="select">' +
                        '<img src="images/icon/'+cont+'.png" alt="Images" class="img-responsive">'+
                        '</span>' +
                        data[i].cat_obra_descricao +
                        '</a>'+
                  '</li>');
                if (cont == 12){
                      break;
                }
                cont++;
            }
         },
         error:function(data){
             console.log(data);
         }
      });   
	},
      listarFiltros:function(){
            $.ajax({
                  url:"https://www.doocati.com.br/tcc/webservice/tipoobra/filtro",
                  type:"GET",
                  dataType:"JSON",
                  success:function(data){
                        console.log(data);
                        var cont = 1;
                        var classe = "";
                        for(var i = 0; i < data.length; i++){
                              switch(cont) {
                              case 1:
                                    classe = "icofont icofont-laptop-alt";
                                    break;
                              case 2:
                                    classe = "icofont icofont-police-car-alt-2";
                                    break;
                              case 3:
                                    classe = "icofont icofont-building-alt";
                                    break;
                              case 4:
                                    classe = "icofont icofont-ui-home";
                                    break;
                              case 5:
                                    classe = "icofont icofont-animal-dog";
                                    break;
                              case 6:
                                    classe = "icofont icofont-nurse";
                                    break;
                              case 7:
                                    classe = "icofont icofont-hockey";
                                    break;
                              case 8:
                                    classe = "icofont icofont-burger";
                                    break;
                              case 9:
                                    classe = "icofont icofont-girl-alt";
                                    break;
                              }
                              $("#categorias-filtros").append('<li><a href="javascript:;" id="cate_'+data[i].cat_obra_id+'" onClick="FiltrarCategoria('+data[i].cat_obra_id+')" ><i class="'+classe+'"></i>'+data[i].cat_obra_descricao+'<span>('+data[i].total+')</span></a></li>');
                        if (cont == 12){
                              break;
                        }
                        cont++;
                        
                        }
                  },
                  error:function(data){
                        console.log(data);
                  }
            });
      }
}