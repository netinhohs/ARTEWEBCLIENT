var googleUser = {};
  var startApp = function() {
    gapi.load('auth2', function(){
      auth2 = gapi.auth2.init({
        client_id: '150179571628-gqimt42o8pufcltqj8ic7prtjua3hduq.apps.googleusercontent.com',
        cookiepolicy: 'single_host_origin',
      });
      attachSignin(document.getElementById('btnLoginGoogle'));
    });
  };

  function attachSignin(element) {
    console.log(element.id);
    auth2.attachClickHandler(element, {},
    function(googleUser) {
      console.log(googleUser.getBasicProfile().getName());
    }, function(error) {
      alert(JSON.stringify(error, undefined, 2));
    });
  }