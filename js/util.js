function Logout(){
	Cookies.remove('usuario');
    signOut();
	window.location.href = 'index.php';
}

function UsuarioLogado(){
	if(Cookies.get("usuario") > 0){
		return true;
	}
	else{
		return false;
	}
}

function getUsuarioLogado(){
	return Cookies.get("usuario");
}

function setUsuarioLogado(id){
	Cookies.set('usuario', id);
}

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
	if(auth2 !== null && auth2 !== 'undefined'){
		auth2.signOut().then(function () {
        console.log('User signed out.');
		});
	}
}

function onLoad() {
    gapi.load('auth2', function() {
        gapi.auth2.init();
    });
}

$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

function mascara(o,f){
    v_obj=o
    v_fun=f
    setTimeout('execmascara()',1)
}

function execmascara(){
    v_obj.value=v_fun(v_obj.value)
}

function masktelefone(v) {
    v = v.replace( /\D/g , ""); 
    v = v.replace( /^(\d\d)(\d)/g , "($1) $2"); 
    if(v.length <= 13){
        v = v.replace( /(\d{4})(\d)/ , "$1-$2"); 
    }else{
        v = v.replace( /(\d{5})(\d)/ , "$1-$2"); 
    }
    return v;
}


function maskdatanascimento(v) {
    v = v.replace( /\D/g , ""); 
    v = v.replace( /(\d{2})(\d)/ , "$1/$2"); 
    v = v.replace( /(\d{2})(\d)/ , "$1/$2"); 
    return v;
}

function maskcep(v) {
    v = v.replace( /\D/g , ""); 
   // v = v.replace( /(\d{2})(\d)/ , "$1.$2"); 
    v = v.replace( /(\d{5})(\d)/ , "$1-$2"); 
    return v;
}
function maskcpfCnpj(v){
     
    v=v.replace(/\D/g,"")
 
    if (v.length < 13) {
        v=v.replace(/(\d{3})(\d)/,"$1.$2")
        v=v.replace(/(\d{3})(\d)/,"$1.$2")
        v=v.replace(/(\d{3})(\d{1,2})$/,"$1-$2")
 
    } else { 
        v=v.replace(/^(\d{2})(\d)/,"$1.$2")
        v=v.replace(/^(\d{2})\.(\d{3})(\d)/,"$1.$2.$3")
        v=v.replace(/\.(\d{3})(\d)/,".$1/$2")
        v=v.replace(/(\d{4})(\d)/,"$1-$2")
    }
 
    return v
 
}
function masknumber(v) {
    v = v.replace( /\D/g , ""); 
    return v;
}

function maskcpf(v){

    v=v.replace(/\D/g,"")
    v=v.replace(/(\d{3})(\d)/,"$1.$2")
    v=v.replace(/(\d{3})(\d)/,"$1.$2")
    v=v.replace(/(\d{3})(\d{1,2})$/,"$1-$2")
    return v

}   

function maskcnpj(v){

    v=v.replace(/\D/g,"")
    v=v.replace(/^(\d{2})(\d)/,"$1.$2")
    v=v.replace(/^(\d{2})\.(\d{3})(\d)/,"$1.$2.$3")
    v=v.replace(/\.(\d{3})(\d)/,".$1/$2")
    v=v.replace(/(\d{4})(\d)/,"$1-$2")
    return v
}

function maskcartao(v){

    v=v.replace(/\D/g,"")
    v=v.replace(/(\d{4})(\d)/,"$1 $2")
    v=v.replace(/(\d{4})(\d)/,"$1 $2")
    v=v.replace(/(\d{4})(\d)/,"$1 $2")
    v=v.replace(/(\d{4})(\d)/,"$1 $2")
    return v
}

function maskdata(v){

    v=v.replace(/\D/g,"")
    v=v.replace(/(\d{2})(\d)/,"$1/$2")
    v=v.replace(/(\d{2})(\d)/,"$1/$2")
    return v
}

function maskdatahora(v){

    v=v.replace(/\D/g,"")
    v=v.replace(/(\d{2})(\d)/,"$1:$2")
    v=v.replace(/(\d{2})(\d)/,"$1 $2")
    v=v.replace(/(\d{2})(\d)/,"$1/$2")
    v=v.replace(/(\d{2})(\d)/,"$1/$2")
    return v
}

