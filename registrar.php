<!DOCTYPE html>
<html lang="pt-BR">
  
	<?php include 'includes/head.php'; ?>

  <body>
	
	<?php include 'includes/topo.php'; ?>

	<section id="main" class="clearfix user-page">
		<div class="container">
			<div class="row text-center">		
				<div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
					<div class="user-account">
						<h2>Criar nova conta</h2>
						<form id="form1">
							<div class="form-group">
								<input type="text" required name="usu_nome" id="usu_nome" class="form-control" placeholder="Nome" >
							</div>
							<div class="form-group">
								<input type="email" required name="usu_email" id="usu_email" class="form-control" placeholder="Email">
							</div>
							<div class="form-group">
								<input type="password" required name="usu_senha" id="usu_senha" class="form-control" placeholder="Senha">
							</div>
							<div class="form-group">
								<input type="password" required name="confirm" id="confirm" class="form-control" placeholder="Confirmar Senha">
							</div>
							
							<div class="checkbox">
								<label class="pull-left checked" for="termo"><input type="checkbox" name="termo" id="termo"> Cadastrando - se em nosso site você esta aceitando nosso termo de uso. </label>
							</div>
							<button type="button" id="btnRegistrar" class="btn">Registrar</button>	
						</form>							
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<?php include 'includes/footer.php'; ?>
	
    <?php include 'includes/scripts.php'; ?>
    <?php include 'includes/verifica-menu.php'; ?>
    <script src="controller/usuario.js"></script>

  </body>
  <script type="text/javascript">
  if(UsuarioLogado() == true){
		window.location.href = 'index.php';
	}

  	$("#btnRegistrar").on("click", function(){
  		
  		$("#form1").validate({
  			rules: {
			    confirm: {
			      equalTo: "#usu_senha"
			    }
			},
	  		highlight: function (e, ec, vc) { 
                $(e).parents("div.form-group").addClass("has-error"); 
            }, 
	        unhighlight: function (e, ec, vc) { 
	                  $(e).parents(".has-error").removeClass("has-error"); 
	        }
	  	});

	  	if($("#form1").valid() == true){
	  		usuario.registroRapido();
  		}
  	});

  </script>

</html>