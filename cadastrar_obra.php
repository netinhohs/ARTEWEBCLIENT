<!DOCTYPE html>
<html lang="pt-BR">
	  
	<?php include 'includes/head.php'; ?>

  <body>
	
	<?php include 'includes/topo.php'; ?>

	
	<section id="main" class="clearfix ad-post-page">
		<div class="container">

			<div class="breadcrumb-section">
				<ol class="breadcrumb">
					<li><a href="index.php">Home</a></li>
					<li>Cadastro de obra</li>
				</ol>
				<h2 class="title">Passo I</h2>
			</div>
				
				
			<div id="ad-post">
				<div class="row category-tab">	
					<div class="col-md-6 col-sm-6">
						<div class="section cat-option select-category post-option">
							<h4>Selecione a Categoria</h4>
							<ul role="tablist" id="listCategorias">
							</ul>
						</div>
					</div>
					
					<div class="col-md-6 col-sm-6">
						<div class="section next-stap post-option">
							<h2>Cadastre sua obra em <span>30 segundos</span></h2>
							<p>Por favor, NÃO cadastre obras duplicadas. Cadastros duplicados serão considerados spam e serão deletados, o usuário poderá ser penalizado</p>
							<div class="btn-section">
								<a href="javascript:;" id="nextPass" class="btn">Proximo</a>
								<a href="index.php" class="btn-info">ou Cancelar</a>
							</div>
						</div>
					</div>
				</div>
			</div>				
		</div>
	
	<?php include 'includes/footer.php'; ?>
	
   	<?php include 'includes/scripts.php'; ?>
   	<?php include 'includes/verifica-menu.php'; ?>
	<script src="controller/usuario.js"></script>
	<script src="controller/categoria.js"></script>
  </body>
   <script type="text/javascript">
   	categoria.listar();
	Cookies.remove('chosenCat');

	$("#nextPass").on('click', function(){
		if(Cookies.get('chosenCat') !== undefined){
			window.location.href = 'cadastrar_obra2.php';
		}else{
			alert('Selecione uma categoria');
		}
	});

	function CateogiaAdd(img, id, nome){
		Cookies.set('chosenCat', { img: img, id: id, nome: nome });
	}
   </script>

</html>