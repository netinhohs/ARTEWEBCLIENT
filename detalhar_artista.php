<!DOCTYPE html>
<html lang="pt-BR">
<?php $naomostrarMap = "S"; ?>
<?php include 'includes/head.php'; ?>
  <body>

	<?php include 'includes/topo.php'; ?>

	<!-- main -->
	<section id="main" class="clearfix details-page">
		<div class="container">
			<div class="breadcrumb-section">
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li><a href="index.php">Home</a></li>
					<li>Artista</li>
				</ol><!-- breadcrumb -->						
				<h2 class="title">Detalhamento</h2>
			</div>
			<div class="banner">
			</div>

			<div class="section slider">					
				<div class="row">
					<div class="col-md-7">
						<div id="product-carousel" class="carousel slide" data-ride="carousel">
							<div class="carousel-inner" role="listbox">
								<div class="item active">
									<div class="carousel-image" id="img_artista">
									</div>
								</div>
							</div>
						</div>
					</div>	
					<input type="hidden" name="artista_id" id="artista_id" value="<?php echo $_GET['artista_id']; ?>">
					<div class="col-md-5">
						<div class="slider-text">
							<h3 class="title" id="txtNome">Nome do artista</h3>
							<div id="dspRank"></div>
							<p><span>Obras Cadastradas: <a href="#" id="txtObra">22</a></span>
							<span> Desde:<a href="#" class="time"> 24/28/2012 </a></span></p>
							<div class="short-info" style="margin-bottom: 0;">
								<h4>Informações</h4>
								<p><strong>E-Mail: </strong><font id="usu_email"> </font></p>
								<p><strong>Sexo: </strong><font id="usu_sexo"> </font></p>
								<p style="margin-bottom: 0;"><strong>Data de Nascimento: <font id="usu_nascimento"> </font></p>
							</div>
							
							<div class="contact-with">
								<h4 style="margin-top: 0;">Contatos </h4>
								<span class="btn btn-red show-number">
									<i class="fa fa-phone-square"></i>
									<span class="hide-text">Click para mostrar</span> 
									<span class="hide-number" id="txtTells">(81) - 3446-1185 / (81) - 99657-4435 </span>
								</span>
								<a href="#" class="btn"><i class="fa fa-envelope-square"></i>Reply by email</a>
							</div>
							
							<div class="social-links">
								<h4>Suas redes sociais</h4>
								<ul class="list-inline">
									<li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
									<li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
									<li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>
									<li><a href="#"><i class="fa fa-tumblr-square"></i></a></li>
								</ul>
							</div>						
						</div>
					</div>				
				</div>				
			</div>

			<div class="description-info">
				<div class="row">
					<div class="col-md-5">
						<div class="description">
							<h4>Descrição</h4>
							<p id="txtDescricao"></p>
						</div>
					</div>
					<div class="col-md-7">
						<div class="description">
							<h4>MAPA</h4>
							<div id="mapa" style="height: 394px;"></div>
						</div>
					</div>
				</div>
			</div>	
			<div class="recommended-info">
				<div class="row">
					<div class="col-sm-5 text-center">
						<div class="recommended-cta" id="dspAvaliar" style="display:none;">		
							<div class="cta">		
								<div class="col-sm-12">
									<div></div>
									<div class="form-group" style="margin-bottom: 5px;">
										<label for="ava_descricao" style="float: left; margin-right: 20px;">Avaliação:</label>
										<section id="star" style="padding: 0; position: relative;">
											<input type="radio" name="ava_nota" class="rating" value="1" />
											<input type="radio" name="ava_nota" class="rating" value="2" />
											<input type="radio" name="ava_nota" class="rating" value="3" />
											<input type="radio" name="ava_nota" class="rating" value="4" />
											<input type="radio" name="ava_nota" class="rating" value="5" />
										</section>
										<textarea class="form-control" placeholder="Deixe seu comentario aqui." id="ava_descricao" name="ava_descricao" rows="2"></textarea>
									</div>
									<button class="btn btn-success" id="btnAvaliar" style="float: right;">Ok</button>
								</div>
							</div>
						</div>
						<div class="recommended-cta">					
							<div class="cta">		
								<div class="col-sm-12" id="showAvaliacoes">

								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-7">				
						<div class="section recommended-ads">
							<div class="featured-top">
								<h4>Obras desse artista</h4>
							</div>
							<div id="minhasObras">

                            </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<?php include 'includes/footer.php'; ?>


    <?php include 'includes/scripts.php'; ?>
	<?php include 'includes/verifica-menu.php'; ?>
	<script src="controller/artista.js"></script>
	<script src="controller/avaliacao.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBeKaWTSV6BS8kQIB_hEpnpNL4O6bNmDyY&amp;sensor=false"></script>
    <script src="js/artistamap.js"></script>
  </body>
  <script type="text/javascript">
  	$("#btnAvaliar").on("click", function(){
  		if($("input[name='ava_nota']:checked").val() == undefined){
  			bootbox.alert("A nota é obrigatório");
  		} else{
  			avaliacao.inserir();
  		}
  	});
  	$('#star').rating();

	if(UsuarioLogado()){
		$("#dspAvaliar").show();
	}

	 $("#btnPesquisar").on("click", function(){
		if($("#txtPesquisar").val() != ""){
			window.location.href = 'listar_artistas.php?getpesquisa='+$("#txtPesquisar").val();
		}
	});

	function carregarPontos() {

		$.ajax({
            url:"https://www.doocati.com.br/tcc/webservice/artistaatelie/"+$("#artista_id").val(),
            type:"GET",
            dataType:"JSON",
            success:function(data){
                console.log(data);
                for(var i = 0; i < data.length; i++){
					var latlngbounds = new google.maps.LatLngBounds();
					var marker = new google.maps.Marker({
						position: new google.maps.LatLng(data[i].ate_latitude, data[i].ate_longitude),
						title: data[i].ate_endereco+' Nº '+data[i].ate_numero,
						map: map,
						icon: 'js/ponto2.png'

					});

					latlngbounds.extend(marker.position);
					map.fitBounds(latlngbounds);
				}
            },
            error:function(data){
                console.log(data);
            }
        });	
	}
	
	carregarPontos();
    
	<?php if($_GET['artista_id'] != ""){ ?>
    	avaliacao.listById(<?php echo $_GET['artista_id']; ?>);
    	avaliacao.rank(<?php echo $_GET['artista_id']; ?>);
        artista.detalharArtista(<?php echo $_GET['artista_id']; ?>);
        artista.listarObraById(<?php echo $_GET['artista_id']; ?>);
    <?php } ?>
  </script>

</html>