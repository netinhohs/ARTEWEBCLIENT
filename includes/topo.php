<header id="header" class="clearfix">
   <nav class="navbar navbar-default">
      <div class="container">
         <!-- navbar-header -->
         <div class="navbar-header"style="height: 90px;">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            	<span class="sr-only">Toggle navigation</span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php"><img class="img-responsive" style="width:200px; height:auto;" src="images/logo-arteson.png" alt="logo-arteson"></a>
         </div>
         <!-- /navbar-header -->
        <!--
        	<div class="navbar-left">
				<div class="collapse navbar-collapse" id="navbar-collapse">
					<ul class="nav navbar-nav">
						<li class="active dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Home <span class="caret"></span></a>
							<ul class="dropdown-menu">
							<li><a href="index-2.html">Home Default </a></li>
							<li><a href="index-one.html">Home Page V-1</a></li>
							<li class="active"><a href="index-two.html">Home Page V-2</a></li>
							<li><a href="index-three.html">Home Page V-3</a></li>
							<li><a href="index-car.html">Home Page V-4<span class="badge">New</span></a></li>
							<li><a href="index-car-two.html">Home Page V-5<span class="badge">New</span></a></li>
							</ul>
						</li>
						<li><a href="categories.html">Category</a></li>
						<li><a href="details.html">all ads</a></li>
						<li><a href="faq.html">Help/Support</a></li> 
						<li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Pages <span class="caret"></span></a>
							<ul class="dropdown-menu">
							<li><a href="about-us.html">ABout Us</a></li>
							<li><a href="contact-us.html">Contact Us</a></li>
							<li><a href="ad-post.html">Ad post</a></li>
							<li><a href="ad-post-details.html">Ad post Details</a></li>
							<li><a href="categories-main.html">Category Ads</a></li>
							<li><a href="details.html">Ad Details</a></li>
							<li><a href="my-ads.html">My Ads</a></li>
							<li><a href="my-profile.html">My Profile</a></li>
							<li><a href="favourite-ads.html">Favourite Ads</a></li>
							<li><a href="archived-ads.html">Archived Ads</a></li>
							<li><a href="pending-ads.html">Pending Ads</a></li>
							<li><a href="delete-account.html">Close Account</a></li>
							<li><a href="published.html">Ad Publised</a></li>
							<li><a href="signup.html">Sign Up</a></li>
							<li><a href="signin.html">Sign In</a></li>
							<li><a href="faq.html">FAQ</a></li> 
							<li><a href="coming-soon.html">Coming Soon <span class="badge">New</span></a></li>
							<li><a href="pricing.html">Pricing<span class="badge">New</span></a></li>
							<li><a href="500-page.html">500 Opsss<span class="badge">New</span></a></li>
							<li><a href="404-page.html">404 Error<span class="badge">New</span></a></li>
							</ul>
						</li>
					</ul>
				</div>
        	</div>
         -->
        	<div class="nav-right"style="margin-top: 3%;">
          
	         <ul class="sign-in" id="menuDeslogado" style="display:none;">
            	<li><i class="fa fa-user"></i></li>
            	<li><a href="login.php"> Login </a></li>
            	<li><a href="registrar.php">Registrar</a></li>
	         </ul>
           <ul class="sign-in" id="menuLogado" style="display:none;">
              <li><i class="fa fa-user"></i></li>
              <li><a href="perfil.php"> Perfil </a></li>
              <li><a onClick="Logout();" href="javascript:;">Logout</a></li>
           </ul>       

          	<a href="cadastrar_obra.php" id="menuCadastraObra" class="btn">Criar obra</a>
        	</div>
      </div>
   </nav>
</header>