<!DOCTYPE html>
<html lang="pt-BR">
  
  <?php include 'includes/head.php'; ?>
  <link rel="stylesheet" href="css/dropzone.css">
  <body>
	
	<?php include 'includes/topo.php'; ?>

	<section id="main" class="clearfix  ad-profile-page">
		<div class="container">
		
			<div class="breadcrumb-section">
				<ol class="breadcrumb">
					<li><a href="index.php">Home</a></li>
					<li>Conta</li>
				</ol>
				<h2 class="title">Meu Perfil</h2>
			</div>
			 
			<div class="ad-profile section">	
				<div class="user-profile">
					<div class="user-images" style="margin-top: 16px;">
						<img id="dspImg" alt="User Images" class="img-responsive" style="width: 154px;">
					</div>
					<div class="user">
						<h2>Olá, <a href="#"><font id="dspNome"></font></a></h2>
						<h5 id="dspMsgCadastro" style="display:none;"> </h5>
					</div>

					<div class="favorites-user">
						<div class="my-ads">
							<a id="obras" href="minhas_obras.php"></a>
						</div>
						<div class="favorites">
							<a href="#">18<small>Meus favoritos</small></a>
						</div>
					</div>								
				</div>
						
				<ul class="user-menu">
					<li class="active"><a href="perfil.php">Perfil</a></li>
					<li><a href="minhas_obras.php">Minhas obras</a></li>
					<li><a href="meu_atelie.php">Meus Ateliês </a></li>
					<li><a href="#">Meus favoritos</a></li>
					<li><a href="deletar_perfil.php">Inativar conta</a></li>
				</ul>
			</div>

			<div id="msgErro" class="alert alert-danger alert-dismissible" role="alert" style="display: none;">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			    <strong>Erro!</strong> <font id="txtErro">texto aqui</font>.
			</div>
			<div id="msgOk" class="alert alert-success alert-dismissible" role="alert" style="display: none;">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			    <strong>Sucesso!</strong> <font id="txtOk">texto aqui</font>.
			</div>
			<div class="profile section">
				<div class="row">
					<div class="col-sm-8">
						<div class="user-pro-section">
							<form id="form1">
								<input type="hidden" name="usu_cadastro_completo" id="usu_cadastro_completo">
								<div class="profile-details section">
									<h2>Detalhe do Perfil</h2>
									<div class="form-group">
										<label class="control-label" for="usu_nome">Nome <span class="required"> * </span></label>
										<input type="text" required id="usu_nome" name="usu_nome" class="form-control">
									</div>

									<div class="form-group">
										<label class="control-label" for="usu_email">Email <span class="required"> * </span></label>
										<input type="email" required id="usu_email" name="usu_email" readonly="true" class="form-control">
									</div>
									<div class="adpost-details" style="display:none;" id="dspSexo">
										<div class="form-group select-condition">
											<label class="control-label">Sexo <span class="required"> * </span></label>
												<input type="radio" checked name="usu_genero" value="M" id="masc"> <label for="masc">Masculino </label>
												<input type="radio" name="usu_genero" value="F" id="fem"> <label for="fem">Feminino</label>	
										</div>
									</div>

									<div class="form-group" id="dspCpf">
										<label class="control-label" for="usu_cpf">CPF <span class="required"> * </span></label>
										<input type="text" id="usu_cpf" maxlength="14" required onkeypress="mascara(this,maskcpf)" name="usu_cpf" class="form-control">
									</div>

									<div class="form-group">
										<label class="control-label" for="usu_telefone">Telefone </label>
										<input type="text" id="usu_telefone" maxlength="15" name="usu_telefone" class="form-control">
									</div>

									<div class="form-group">
										<label class="control-label" for="usu_celular">Celular </label>
										<input type="text" id="usu_celular" maxlength="15" name="usu_celular" class="form-control">
									</div>
									<div class="form-group">
										<label class="control-label" for="usu_descricao">Descrição</label>
										<textarea class="form-control" required id="usu_descricao" name="usu_descricao" rows="5"></textarea>
									</div>
									<div class="form-group">
									<label class="control-label" for="my-dropzone">Troque sua foto</label>
										<div style="float: left;" id="my-dropzone" action="https://www.doocati.com.br/tcc/client/uploadFotoPerfil.php?id=2" class="dropzone"></div>
									</div>
								</div>

								<div class="change-password section">
									<h2>Trocar senha</h2>
									<div class="form-group">
										<label>Nova senha</label>
										<input type="password" name="usu_senha" id="usu_senha" class="form-control">	
									</div>
									
									<div class="form-group">
										<label>Confirmar senha</label>
										<input type="password" name="confirm" id="confirm" class="form-control">
									</div>															
								</div>
								<input type="hidden" id="usu_imagem" name="usu_imagem" />
							</form>
							<a href="#" id="btnAtualizarPefil" class="btn">Atualizar perfil</a>
						</div>
					</div>

					<div class="col-sm-4 text-center">
						<div class="recommended-cta">					
							<div class="cta">				
								<div class="single-cta">
									<div class="cta-icon icon-secure">
										<img src="images/icon/13.png" alt="Icon" class="img-responsive">
									</div>
									<h4>Site seguro</h4>
									<p>Não se preocupe, site 100% seguro
								</div>

								<div class="single-cta">
									<div class="cta-icon icon-support">
										<img src="images/icon/14.png" alt="Icon" class="img-responsive">
									</div>
									<h4>Suporte 24h</h4>
									<p>Nos contate caso necessite
								</div>

								<div class="single-cta">
									<div class="cta-icon icon-trading">
										<img src="images/icon/15.png" alt="Icon" class="img-responsive">
									</div>
									<h4>Mapa</h4>
									<p>Cadastre Atilies para aparecer no Mapa
								</div>

								<div class="single-cta">
									<h5>Precisa de Ajuda?</h5>
									<p><span>Entre em contato</span><a href="tellto:08048100000"> suporte@arteson.com.br</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>				
		</div>
	</section>
	
	<?php include 'includes/footer.php'; ?>

	<?php include 'includes/scripts.php'; ?>
	<?php include 'includes/verifica-menu.php'; ?>
	<script src="controller/usuario.js"></script>
	<script src="js/dropzone.js"></script>
  </body>

  <script type="text/javascript">
  	Dropzone.autoDiscover = false;
		$("#my-dropzone").dropzone({
			addRemoveLinks: true,
			renameFilename: function (filename) {
				return 'capas/'+filename;
			},
			init: function() {
					this.on("success", function(file, Data) {
					var nomeImg = file.previewElement.querySelector("[data-dz-name]");
						$("#usu_imagem").val(nomeImg.textContent);
				});
				this.on("removedfile", function(file) {
					var nomeImg = file.previewElement.querySelector("[data-dz-name]");
					$("#usu_imagem[value='"+nomeImg.textContent+"']").remove();
					console.log('tentou');
				});
			},
			
		});

  	if(UsuarioLogado() == false){
		window.location.href = 'login.php?acesso=1';
	}
	else{
		usuario.menuPerfil(getUsuarioLogado());
		usuario.detalhar(getUsuarioLogado());
	}

	<?php if(!empty($_GET['sucess'])){ ?>
        $("#txtOk").html("Atualização realizada!.");
        $("#msgOk").show();
    <?php } ?>

	$("#btnAtualizarPefil").on("click",function(e){
	  	
	  	$("#form1").validate({
  			rules: {
			    confirm: {
			      equalTo: "#usu_senha"
			    }
			},
	  		highlight: function (e, ec, vc) { 
                $(e).parents("div.form-group").addClass("has-error"); 
            }, 
	        unhighlight: function (e, ec, vc) { 
                $(e).parents(".has-error").removeClass("has-error"); 
	        }
	  	});

	  	if($("#form1").valid() == true){
	  		e.preventDefault();
	  		usuario.atualizarPerfil(getUsuarioLogado());
  		}
  	});
  </script>
  
<script type = "text/javascript" src="js/jquery.mask.js"></script>
<script type = "text/javascript">
	$("div#myId").dropzone({ url: "capas/" });
    $("#usu_celular").mask("(99) 99999-9999");    
    $("#usu_telefone").mask("(99) 9999-9999");
	$("#usu_cpf").mask("999.999.999-99");
	 
</script>


</html>