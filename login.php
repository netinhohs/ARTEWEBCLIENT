<!DOCTYPE html>
<html lang="pt-BR">
  
	<?php include 'includes/head.php'; ?>

  <body>
	
	<?php include 'includes/topo.php'; ?>
	
	<section id="main" class="clearfix user-page">
		<div class="container">
			<div class="row text-center">	
				<div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
					<div id="msgErro" class="alert alert-danger alert-dismissible" role="alert" style="display:none;">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					    <strong>Erro!</strong> <font id="txtErro">texto aqui</font>.
					</div>
					<div id="msgOk" class="alert alert-success alert-dismissible" role="alert" style="display:none;">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					    <strong>Sucesso!</strong> <font id="txtOk">texto aqui</font>.
					</div>
					<div class="user-account showLogin">
						<h2>Login do usuário</h2>

						<form id="formLogin">
							<div class="form-group">
								<input type="email" required name="email" id="email"class="form-control" placeholder="Email" >
							</div>
							<div class="form-group">
								<input type="password" required name="senha" id="senha"class="form-control" placeholder="Senha" >
							</div>
						</form>
							<button type="button" id="btnLogin" href="#" class="btn">Login</button>
						<div class="user-option">
							<div class="checkbox pull-left">
								<label for="logged"><input type="checkbox" name="logged" id="logged"> lembrar - me </label>
							</div>
							<div class="pull-right forgot-password">
								<a href="javascript:;" id="btnEsqueciSenha">Esqueci senha</a>
							</div>
						</div>
					</div>
					<div class="user-account showEsqueci" style="display:none;">
						<h2>Esqueci a senha</h2>

						<form id="formEsqueci">
							<div class="form-group">
								<input type="email" name="forgot" id="forgot" required class="form-control" placeholder="Email" >
							</div>
						</form>
							<button type="button" id="btnEnviarSenha" href="#" class="btn">Enviar</button>
					
						<div class="user-option">
							<div class="pull-left forgot-password">
								<a href="javascript:;" id="btnVoltar"> Voltar</a>
							</div>
						</div>
					</div>
					<div class="g-signin2" style="padding-top: 8px;" data-width="auto" data-longtitle="true" data-onsuccess="onSignIn"></div>
					<a  href="registrar.php" class="btn-primary">Criar novo usuário</a>
				</div>
			</div>
		</div>
	</section>
	  
	<?php include 'includes/footer.php'; ?>

    <?php include 'includes/scripts.php'; ?>
    <?php include 'includes/verifica-menu.php'; ?>
   	<script src="controller/usuario.js"></script>
  </body>

  	<script type="text/javascript">

	<?php if(!empty($_GET['acesso'])){ ?>
        $("#txtErro").html("Você precisa estar logado para executar essa ação.");
        $("#msgErro").show();
    <?php }elseif(!empty($_GET['create'])){ ?>
    	$("#txtOk").html("Cadastro criado. você receberá um E-mail com mais informações.");
        $("#msgOk").show();
	<?php } ?>

	  if(UsuarioLogado() == true){
		  window.location.href = 'index.php';
	  }

  		function onSignIn(googleUser) {
		  	var profile = googleUser.getBasicProfile();
		  	var obj = {};
		  	obj.usu_nome = profile.getName();
		  	obj.usu_email = profile.getEmail();
		  	obj.usu_imagem = profile.getImageUrl();
		  	obj.usu_id_google = profile.getId();
			
			usuario.google(obj); 
		}

	  	$("#btnLogin").on("click",function(){
	  	
		  	$("#formLogin").validate({
		  		highlight: function (e, ec, vc) { 
	                $(e).parents("div.form-group").addClass("has-error"); 
	            }, 
		        unhighlight: function (e, ec, vc) { 
		                  $(e).parents(".has-error").removeClass("has-error"); 
		        }
		  	});

	  		if($("#formLogin").valid() == true){
	  			var email = $("#email").val();
	            var senha = $("#senha").val();
	            usuario.login(email, senha);
	  		}
	  	});

	  	$("#btnEsqueciSenha").on("click", function(){
  			$(".showLogin").hide();
  			$(".showEsqueci").show();
	  	});

	  	$("#btnVoltar").on("click", function(){
  			$(".showEsqueci").hide();
  			$(".showLogin").show();
	  	});

	  	$("#btnEnviarSenha").on("click",function(){

	  		$("#formEsqueci").validate({
		  		highlight: function (e, ec, vc) { 
	                $(e).parents("div.form-group").addClass("has-error"); 
	            }, 
		        unhighlight: function (e, ec, vc) { 
                    $(e).parents(".has-error").removeClass("has-error"); 
		        }
		  	});

	  		if($("#formEsqueci").valid() == true){
	  		
	  		}
	  	});
  	</script>

</html>