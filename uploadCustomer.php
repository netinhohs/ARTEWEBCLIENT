<?php 
$data = array();
		try{
			$mUploadDir = "img/";
			$data['debug'] = is_dir($mUploadDir);
			$data['mydir'] = $_SERVER['PHP_SELF'];
			foreach($_FILES as $mFile) {
				if (!empty($mFile["size"])) {
					$fileSize = ($mFile["size"] / 1024) / 1024;
					
					if ($fileSize > 2) {
						$data["success"] = false;
						$data["error"] = "Arquivo maior do que 2 MB";
						return $data;
					}
				} else {
					$data["success"] = false;
					$data["error"] = "Arquivo maior do que 2 MB ou corrompido";
					return $data;
				}
				$extencions = array("jpg","gif","png");
				$type = explode(".",$mFile["name"]);
				if(move_uploaded_file($mFile["tmp_name"], $mUploadDir.$mFile["name"])) {
					return $_FILES;
				}else{
					return "diretorio não existe";
				}
			}
		}catch(Exception $e){
			$data["success"] = false;
			$data["error"] = $e->getMessage();
		}

?>