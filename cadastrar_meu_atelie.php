<!DOCTYPE html>
  
<html lang="pt-BR">
  
  <?php include 'includes/head.php'; ?>

  <body>
	
	<?php include 'includes/topo.php'; ?>
	
	<section id="main" class="clearfix myads-page">
		<div class="container">

			<div class="breadcrumb-section">
			
				<ol class="breadcrumb">
					<li><a href="index-2.html">Home</a></li>
					<li>Conta</li>
				</ol><!-- breadcrumb -->						
				<h2 class="title">Meus Ateliês</h2>
			</div><!-- banner -->

			<div class="ad-profile section">	
					<div class="user-profile">
						<div class="user-images" style="margin-top: 16px;">
							<img id="dspImg" alt="User Images" class="img-responsive" style="width: 154px;">
						</div>
						<div class="user">
                            <h2>Olá, <a href="#"><font id="dspNome"></font></a></h2>
                            <h5 id="dspMsgCadastro" style="display:none;"> </h5>
                        </div>

						<div class="favorites-user">
                            <div class="my-ads">
                                <a id="obras" href="minhas_obras.php"></a>
                            </div>
                            <div class="favorites">
                                <a href="#">18<small>Meus favoritos</small></a>
                            </div>
                        </div>									
					</div>
							
                <ul class="user-menu">
					<li><a href="perfil.php">Perfil</a></li>
					<li><a href="minhas_obras.php">Minhas obras</a></li>
					<li class="active"><a href="meu_atelie.php">Meus Ateliês </a></li>
					<li><a href="#">Meus favoritos</a></li>
					<li><a href="deletar_perfil.php">Inativar conta</a></li>
				</ul>
			
			</div><!-- ad-profile -->			
			
			<div class="ads-info">
				<div class="row">
					<div class="col-sm-8">
						<div class="my-ads section">
							<h2>Cadastrar Ateliês</h2>
								<form id="form1" enctype="multipart/form-data">
									<input type="hidden" name="cat_obra_id" id="cat_obra_id">
									<input type="hidden" name="usu_id" id="usu_id">
									<input type="hidden" name="ate_id" id="ate_id">
									<fieldset>
											
										
										<div class="section seller-info">
											<div class="row form-group">
												<label class="col-sm-3 label-title">Nome Ateliê <span class="required">*</span></label>
												<div class="col-sm-9">
													<input type="text" name="ate_nome" required id="ate_nome" maxlength="255" class="form-control">
												</div>
											</div>
											<div class="row form-group">
												<label class="col-sm-3 label-title">CEP <span class="required">*</span></label>
												<div class="col-sm-9">
													<input type="text" name="ate_cep" required id="ate_cep" maxlength="9" onkeypress="mascara(this,maskcep)" class="form-control">
												</div>
											</div>
											<div class="row form-group">
												<label class="col-sm-3 label-title">Endereço <span class="required">*</span></label>
												<div class="col-sm-6">
													<input type="text" name="ate_endereco" required id="ate_endereco" class="form-control">
												</div>
												<div class="col-sm-3">
													<input type="text" name="ate_numero" required id="ate_numero" class="form-control" placeholder="Numero">
												</div>
											</div>
											<div class="row form-group">
												<label class="col-sm-3 label-title">Complemento</label>
												<div class="col-sm-9">
													<input type="text" name="ate_complemento" id="ate_complemento" class="form-control" placeholder="ex, alekdera House, coprotec, usa">
												</div>
											</div>
											<div class="row form-group">
												<label class="col-sm-3 label-title">Bairro <span class="required">*</span></label>
												<div class="col-sm-9">
													<input type="text" name="ate_bairro" required id="ate_bairro" class="form-control" placeholder="ex, alekdera House, coprotec, usa">
												</div>
											</div>
											<div class="row form-group">
												<label class="col-sm-3 label-title">Cidade <span class="required">*</span></label>
												<div class="col-sm-9">
													<input type="text" name="ate_cidade" required id="ate_cidade" class="form-control" placeholder="ex, alekdera House, coprotec, usa">
												</div>
											</div>
											<div class="row form-group">
												<label class="col-sm-3 label-title">UF <span class="required">*</span></label>
												<div class="col-sm-9">
													<select name="ate_estado" id="ate_estado" required class="form-control ">
														<option value="">Selecione</option>
														<option value="AC">Acre</option>
														<option value="AL">Alagoas</option>
														<option value="AP">Amapá</option>
														<option value="AM">Amazonas</option>
														<option value="BA">Bahia</option>
														<option value="CE">Ceará</option>
														<option value="DF">Distrito Federal</option>
														<option value="ES">Espirito Santo</option>
														<option value="GO">Goiás</option>
														<option value="MA">Maranhão</option>
														<option value="MT">Mato Grosso</option>
														<option value="MS">Mato Grosso do Sul</option>
														<option value="MG">Minas Gerais</option>
														<option value="PA">Pará</option>
														<option value="PB">Paraiba</option>
														<option value="PR">Paraná</option>
														<option value="PE">Pernambuco</option>
														<option value="PI">Piauí</option>
														<option value="RJ">Rio de Janeiro</option>
														<option value="RN">Rio Grande do Norte</option>
														<option value="RS">Rio Grande do Sul</option>
														<option value="RO">Rondônia</option>
														<option value="RR">Roraima</option>
														<option value="SC">Santa Catarina</option>
														<option value="SP">São Paulo</option>
														<option value="SE">Sergipe</option>
														<option value="TO">Tocantis</option>
													</select>
												</div>
			
										</div>
										<div class="checkbox section agreement">
											<button type="button" id="btnCadastrar" class="btn btn-primary">Salvar</button>
										</div>
										
									</fieldset>
								</form>
					</div>

					</div><!-- my-ads -->

					<!-- recommended-cta-->
					<div class="col-sm-4 text-center">
						<!-- recommended-cta -->
						<div class="recommended-cta">					
							<div class="cta">
								<!-- single-cta -->						
								<div class="single-cta">
									<!-- cta-icon -->
									<div class="cta-icon icon-secure">
										<img src="images/icon/13.png" alt="Icon" class="img-responsive">
									</div><!-- cta-icon -->

									<h4>Secure Trading</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
								</div><!-- single-cta -->
								

								<!-- single-cta -->
								<div class="single-cta">
									<!-- cta-icon -->
									<div class="cta-icon icon-support">
										<img src="images/icon/14.png" alt="Icon" class="img-responsive">
									</div><!-- cta-icon -->

									<h4>24/7 Support</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
								</div><!-- single-cta -->
							

								<!-- single-cta -->
								<div class="single-cta">
									<!-- cta-icon -->
									<div class="cta-icon icon-trading">
										<img src="images/icon/15.png" alt="Icon" class="img-responsive">
									</div><!-- cta-icon -->

									<h4>Easy Trading</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
								</div><!-- single-cta -->

								<!-- single-cta -->
								<div class="single-cta">
									<h5>Need Help?</h5>
									<p><span>Give a call on</span><a href="tellto:08048100000"> 08048100000</a></p>
								</div><!-- single-cta -->
							</div>
						</div><!-- cta -->
					</div><!-- recommended-cta-->				
					
				</div><!-- row -->
			</div><!-- row -->
		</div><!-- container -->
	</section>
	
	
	<?php include 'includes/footer.php'; ?>

	<?php include 'includes/scripts.php'; ?>
	<?php include 'includes/verifica-menu.php'; ?>
    <script src="controller/usuario.js"></script>
    <script src="controller/obra.js"></script>
	<script src="controller/atelie.js"></script>
  </body>

  <script type="text/javascript">
		if(UsuarioLogado() == false){
			window.location.href = 'login.php?acesso=1';
		}
		else{
			usuario.menuPerfil(getUsuarioLogado());
		}

		<?php if(isset($_GET["ate_id"])){ ?>
			atelie.detalhar(<?php echo $_GET["ate_id"]; ?>);
		<?php } ?>

		function excluirAtelie(id){
			bootbox.confirm("Tem certeza que deseja deletar a obra?", function(result){ 
				if (result == true){
					atelie.excluir(id);
				} 
			});
		}

  </script>

   <script type="text/javascript">

   $("#btnCadastrar").on("click", function(){
	  $("#form1").validate({
			highlight: function (e, ec, vc) { 
				$(e).parents("div.form-group").addClass("has-error"); 
			}, 
			unhighlight: function (e, ec, vc) { 
				$(e).parents(".has-error").removeClass("has-error"); 
			}
		});
	   	if($("#form1").valid() == true){
			if($("#ate_id").val() != ""){
                atelie.alterar($("#ate_id").val());
            }else{
				atelie.inserir(getUsuarioLogado());
			}
  		}
   });

   $('#ate_cep').on('blur',function(){
	   if($(this).val() != ""){
		   atelie.buscarCepCorreios($(this).val());
	   }
   });
  </script>
</html>