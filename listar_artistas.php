<!DOCTYPE html>
<html lang="en">
  
	<?php include 'includes/head.php'; ?>

  <body>

  	<?php include 'includes/topo.php'; ?>

	<section id="main" class="clearfix category-page main-categories">
		<div class="container">
			<div class="breadcrumb-section">
				<ol class="breadcrumb">
					<li><a href="index.php">Home</a></li>
					<li>Obras</li>
				</ol>						
				<h2 class="title">Lista de artistas</h2>
			</div>
			<div class="banner">
				
				<div class="banner-form banner-form-full">
					<form>
						<input type="text" name="txtPesquisar" id="txtPesquisar" class="form-control" placeholder="Digite o que procura aqui">
						<button type="button" id="btnPesquisar" class="form-control" value="Search">Procurar</button>
					</form>
				</div>
			</div>
	
			<div class="category-info">	
				<div class="row">
					
					<div class="col-md-3 col-sm-4">
						<div class="accordion">
							
							<div class="panel-group" id="accordion">
							 	
								
								<div class="panel-default panel-faq">
									<div class="panel-heading">
										<a data-toggle="collapse" data-parent="#accordion" href="#accordion-one">
											<h4 class="panel-title">Categorias<span class="pull-right"><i class="fa fa-minus"></i></span></h4>
										</a>
									</div>

									<div id="accordion-one" class="panel-collapse collapse in">
										<div class="panel-body">
											<ul id="categorias-filtros">
											</ul>
										</div>
									</div>
								</div>

							</div>
						</div>
					</div>

					<div class="col-sm-8 col-md-9">				
						<div class="section recommended-ads">
							<div class="featured-top">
								<div class="dropdown pull-right">
									<div class="dropdown category-dropdown">
										<h5>Order por:</h5>						
										<a data-toggle="dropdown" href="#"><span class="change-text">Popular</span><i class="fa fa-caret-square-o-down"></i></a>
										<ul class="dropdown-menu category-change">
											<li><a href="#">Nome</a></li>
											<li><a href="#">Melhores Artista</a></li>
										</ul>								
									</div>
								</div>							
							</div>
							<div id="showObras">

								<div class="text-center">
									<ul class="pagination ">
										<li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
										<li><a href="#">1</a></li>
										<li class="active"><a href="#">2</a></li>
										<li><a href="#">3</a></li>
										<li><a href="#">4</a></li>
										<li><a href="#">5</a></li>
										<li><a href="#">...</a></li>
										<li><a href="#">10</a></li>
										<li><a href="#">20</a></li>
										<li><a href="#">30</a></li>
										<li><a href="#"><i class="fa fa-chevron-right"></i></a></li>			
									</ul>
								</div>
							</div>

						</div>
					</div>
				</div>	
			</div>
		</div>
	</section>
	
	
	<section id="something-sell" style="display: none;" class="clearfix parallax-section">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<h2 class="title">Gostaria de publicar sua obra??</h2>
					<h4>Cadastre - se e começe agora!</h4>
					<a href="login.php" class="btn btn-primary">Criar conta</a>
				</div>
			</div>
		</div>
	</section>
	
	<?php include 'includes/footer.php'; ?>
 	
	
    <?php include 'includes/scripts.php'; ?>
   	<?php include 'includes/verifica-menu.php'; ?>
	<script src="controller/categoria.js"></script>
	<script src="controller/artista.js"></script>
  </body>
  <script type="text/javascript">
  categoria.listarFiltros();
  <?php if($_GET['getpesquisa'] == ""){ ?>
  	artista.listarTodos();
  <?php } ?>
  if(UsuarioLogado() == false){
		$("#something-sell").show();
  	}

	function FiltrarCategoria(id){
		artista.filtroCategoria(id);
	}

	<?php if($_GET['getpesquisa'] != ""){ ?>
		artista.filtroLike(	<?php echo ('"'.$_GET['getpesquisa'].'"'); ?>);	
	<?php } ?>	

	$("#btnPesquisar").on("click", function(){
		if($("#txtPesquisar").val() != null){
			artista.filtroLike($("#txtPesquisar").val());
		}
	});

  </script>

</html>