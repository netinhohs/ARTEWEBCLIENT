<!DOCTYPE html>
<html lang="pt-BR">
  
	<?php include 'includes/head.php'; ?>

  <body>
	
	<?php include 'includes/topo.php'; ?>

	<!-- world-gmap -->
	<section id="main" class="clearfix home-two">
		<!-- gmap -->	
		<div id="road_map"></div>		
		
		<div class="container">
			<div class="row">
				<!-- banner -->
				<div class="col-sm-12">
					<div class="banner">						
						<!-- banner-form -->
						<div class="banner-form banner-form-full">
							<form action="#">
								<input type="text" name="txtpesquisa" id="txtpesquisa" class="form-control" placeholder="Digite o que esta procurando..">
								<button type="button" class="form-control" id="btnPesquisa">Pesquisar</button>
							</form>
						</div>						
					</div>
				</div>
			</div>
			
			<!--
			<div class="category-ad text-center">
				<ul class="category-list">	
					<li class="category-item">
						<a href="categories.html">
							<div class="category-icon"><img src="images/icon/1.png" alt="images" class="img-responsive"></div>
							<span class="category-title">Cars & Vehicles</span>
							<span class="category-quantity">(1298)</span>
						</a>
					</li>
					
					<li class="category-item">
						<a href="categories.html">
							<div class="category-icon"><img src="images/icon/2.png" alt="images" class="img-responsive"></div>
							<span class="category-title">Electrics & Gedgets</span>
							<span class="category-quantity">(76212)</span>
						</a>
					</li>
					
					<li class="category-item">
						<a href="categories.html">
							<div class="category-icon"><img src="images/icon/3.png" alt="images" class="img-responsive"></div>
							<span class="category-title">Real Estate</span>
							<span class="category-quantity">(212)</span>
						</a>
					</li>
					
					<li class="category-item">
						<a href="categories.html">
							<div class="category-icon"><img src="images/icon/4.png" alt="images" class="img-responsive"></div>
							<span class="category-title">Sports & Games</span>
							<span class="category-quantity">(972)</span>
						</a>
					</li>
					
					<li class="category-item">
						<a href="categories.html">
							<div class="category-icon"><img src="images/icon/5.png" alt="images" class="img-responsive"></div>
							<span class="category-title">Fshion & Beauty</span>
							<span class="category-quantity">(1298)</span>
						</a>
					</li>
					
					<li class="category-item">
						<a href="categories.html">
							<div class="category-icon"><img src="images/icon/6.png" alt="images" class="img-responsive"></div>
							<span class="category-title">Pets & Animals</span>
							<span class="category-quantity">(76212)</span>
						</a>
					</li>
					
					<li class="category-item">
						<a href="categories.html">
							<div class="category-icon"><img src="images/icon/9.png" alt="images" class="img-responsive"></div>
							<span class="category-title">Home Appliances</span>
							<span class="category-quantity">(1298)</span>
						</a>
					</li>
					
					<li class="category-item">
						<a href="categories.html">
							<div class="category-icon"><img src="images/icon/10.png" alt="images" class="img-responsive"></div>
							<span class="category-title">Matrimony Services</span>
							<span class="category-quantity">(76212)</span>
						</a>
					</li>
					
					<li class="category-item">
						<a href="categories.html">
							<div class="category-icon"><img src="images/icon/11.png" alt="images" class="img-responsive"></div>
							<span class="category-title">Music & Arts</span>
							<span class="category-quantity">(212)</span>
						</a>
					</li>
					
					<li class="category-item">
						<a href="categories.html">
							<div class="category-icon"><img src="images/icon/12.png" alt="images" class="img-responsive"></div>
							<span class="category-title">Miscellaneous </span>
							<span class="category-quantity">(1298)</span>
						</a>
					</li>
					
					<li class="category-item">
						<a href="categories.html">
							<div class="category-icon"><img src="images/icon/7.png" alt="images" class="img-responsive"></div>
							<span class="category-title">Job Openings </span>
							<span class="category-quantity">(124)</span>
						</a>
					</li>
					
					<li class="category-item">
						<a href="categories.html">
							<div class="category-icon"><img src="images/icon/8.png" alt="images" class="img-responsive"></div>
							<span class="category-title">Books & Magazines</span>
							<span class="category-quantity">(972)</span>
						</a>
					</li>					
				</ul>				
			</div>			
			-->
			
			<!-- featureds -->
			<div class="section featureds">
				<div class="row">
					<!-- featured-top -->
					<div class="col-sm-2">
						<div class="featured-top">
								<h4>&nbsp;</h4>
						</div>
					</div>
					<div class="col-sm-10">
						<div class="featured-top">
							<h4>Artistas pertinho de você!!</h4>
						</div>
					</div><!-- featured-top -->
				</div>
				<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-10">
						<div id="showIndex">

						</div>
					</div>
				</div>

				<div class="row">
					<div class="text-center">
						<!-- <ul class="pagination ">
							<li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
							<li><a href="#">1</a></li>
							<li class="active"><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li><a href="#">5</a></li>
							<li><a href="#">...</a></li>
							<li><a href="#">10</a></li>
							<li><a href="#">20</a></li>
							<li><a href="#">30</a></li>
							<li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
						</ul> -->
						<div class="featured-top">
							<a href="listar_artistas.php"><h3 class="item-price" style="font-size: 19px; margin-top: -2px;">Confira mais</h3></a>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</section>
	
	<!-- download -->
	<section id="download" class="clearfix parallax-section">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<h2>Download on App Store</h2>
				</div>
			</div>
			
			<div class="row">
				
				<div class="col-sm-4">
					<a href="#" class="download-app">
						<img src="images/icon/16.png" alt="Image" class="img-responsive">
						<span class="pull-left">
							<span>available on</span>
							<strong>Google Play</strong>
						</span>
					</a>
				</div>

				<div class="col-sm-4">
					<a href="#" class="download-app">
						<img src="images/icon/17.png" alt="Image" class="img-responsive">
						<span class="pull-left">
							<span>available on</span>
							<strong>App Store</strong>
						</span>
					</a>
				</div>

				<div class="col-sm-4">
					<a href="#" class="download-app">
						<img src="images/icon/18.png" alt="Image" class="img-responsive">
						<span class="pull-left">
							<span>available on</span>
							<strong>Windows Store</strong>
						</span>
					</a>
				</div>
			</div>
		</div>
	</section>
	
	<?php include 'includes/footer.php'; ?>
	
	<?php include 'includes/scripts.php'; ?>
	<?php include 'includes/verifica-menu.php'; ?>
	<script src="controller/artista.js"></script>
  </body>
   <script type="text/javascript">
   $( document ).ready(function() {
		 artista.listIndex();

		 $("#btnPesquisa").on("click", function(){
			 if($("#txtpesquisa").val() != ""){
				 window.location.href = 'listar_artistas.php?getpesquisa='+$("#txtpesquisa").val();
			 }
		 });

	});
  
  </script>
</html>