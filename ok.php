<!DOCTYPE html>
<html lang="pt-BR">
  
<?php include 'includes/head.php'; ?>

  <body>
	
	<?php include 'includes/topo.php'; ?>

	<!-- main -->
	<section id="main" class="clearfix published-page">
		<div class="container">
			<div class="row text-center">				
				<div class="col-sm-6 col-sm-offset-3">
					<div class="congratulations">
						<i class="fa fa-check-square-o"></i>
						<h2>Tudo certo!</h2>
						<h4>Sua obra será publicada em alguns instantes.</h4>
					</div>
				</div>
			</div>	
		</div>
	</section>
	
    <?php include 'includes/footer.php'; ?>

    <?php include 'includes/scripts.php'; ?>
    <?php include 'includes/verifica-menu.php'; ?>

  </body>

</html>