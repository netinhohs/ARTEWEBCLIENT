<!DOCTYPE html>
<html lang="pt-BR">

  <?php include 'includes/head.php'; ?>
  <body>

	<?php include 'includes/topo.php'; ?>


	<section id="main" class="clearfix delete-page">
		<div class="container">

			<div class="breadcrumb-section">
				<ol class="breadcrumb">
					<li><a href="index.php">Home</a></li>
					<li>Conta</li>
				</ol>
				<h2 class="title">Meu Perfil</h2>
			</div>

			<div class="ad-profile section">	
				<div class="user-profile">
					<div class="user-images" style="margin-top: 16px;">
						<img id="dspImg" alt="User Images" class="img-responsive" style="width: 154px;">
					</div>
					<div class="user">
						<h2>Olá, <a href="#"><font id="dspNome"></font></a></h2>
						<h5 id="dspMsgCadastro" style="display:none;"> </h5>
					</div>

					<div class="favorites-user">
						<div class="my-ads">
							<a id="obras" href="minhas_obras.php"></a>
						</div>
						<div class="favorites">
							<a href="#">18<small>Meus favoritos</small></a>
						</div>
					</div>								
				</div>
						
				<ul class="user-menu">
					<li><a href="perfil.php">Perfil</a></li>
					<li><a href="minhas_obras.php">Minhas obras</a></li>
					<li><a href="meu_atelie.php">Meus Ateliês </a></li>
					<li><a href="#">Meus favoritos</a></li>
					<li class="active"><a href="deletar_perfil.php">Inativar conta</a></li>
				</ul>
			</div>	
			
			<div class="close-account">
				<div class="row">
					<div class="col-sm-8 text-center">
						<div class="delete-account section">
							<h2>Inativar conta</h2>
						<!--	<h4>Are you sure, you want to delete your account?</h4> -->
							<a href="javascript:;" id="btnCancelar" class="btn">Inativar</a>
						</div>
					</div>

					<div class="col-sm-4 text-center">
						<div class="recommended-cta">					
							<div class="cta">				
								<div class="single-cta">
									<div class="cta-icon icon-secure">
										<img src="images/icon/13.png" alt="Icon" class="img-responsive">
									</div>
									<h4>Site seguro</h4>
									<p>Não se preocupe, site 100% seguro
								</div>

								<div class="single-cta">
									<div class="cta-icon icon-support">
										<img src="images/icon/14.png" alt="Icon" class="img-responsive">
									</div>
									<h4>24/7 suporte</h4>
									<p>Nos contate caso necessite
								</div>

								<div class="single-cta">
									<div class="cta-icon icon-trading">
										<img src="images/icon/15.png" alt="Icon" class="img-responsive">
									</div>
									<h4>Mapa</h4>
									<p>Cadastre Atilies para aparecer no Mapa
								</div>

								<div class="single-cta">
									<h5>Precisa de Ajuda?</h5>
									<p><span>Entre em contato</span><a href="tellto:08048100000"> suporte@arteson.com.br</a></p>
								</div>
							</div>
						</div>
					</div>				
				</div>
			</div>
		</div>
	</section>
	
	<?php include 'includes/footer.php'; ?>

	<?php include 'includes/scripts.php'; ?>
	<?php include 'includes/verifica-menu.php'; ?>
	<script src="controller/usuario.js"></script>
  </body>
  <script type="text/javascript">
    if(UsuarioLogado() == false){
		window.location.href = 'login.php?acesso=1';
	}
	else{
		usuario.menuPerfil(getUsuarioLogado());
		usuario.detalhar(getUsuarioLogado());
	}

    $("#btnCancelar").on("click", function(){
        bootbox.confirm("Tem certeza que deseja inativar sua conta?", function(result){ 
            if (result == true){
                usuario.inativarConta(getUsuarioLogado());
            } 
        });
    });
  </script>
</html>