<!DOCTYPE html>
<html lang="pt-BR">

<?php include 'includes/head.php'; ?>
  <body>

	<?php include 'includes/topo.php'; ?>

	<!-- main -->
	<section id="main" class="clearfix details-page">
		<div class="container">
			<div class="breadcrumb-section">
				<!-- breadcrumb -->
				<ol class="breadcrumb">
					<li><a href="index.php">Home</a></li>
					<li>Obra</li>
				</ol><!-- breadcrumb -->						
				<h2 class="title">Detalhe da obra</h2>
			</div>
			<div class="banner">
				
				<div class="banner-form banner-form-full">
					<form>
						<input type="text" name="txtPesquisar" id="txtPesquisar" class="form-control" placeholder="Digite o que procura aqui">
						<button type="button" id="btnPesquisar" class="form-control" value="Search">Procurar</button>
					</form>
				</div>
			</div>

			<div class="section slider">					
				<div class="row" style="min-height: 635px;">
					<div class="col-md-7">
						<div id="product-carousel" class="carousel slide" data-ride="carousel">

                            <ol class="carousel-indicators" id="ImgGrande">
							</ol>
							<div class="carousel-inner" role="listbox" id="ImgPequena">
							</div>
							<a class="left carousel-control" href="#product-carousel" role="button" data-slide="prev">
								<i class="fa fa-chevron-left"></i>
							</a>
							<a class="right carousel-control" href="#product-carousel" role="button" data-slide="next">
								<i class="fa fa-chevron-right"></i>
							</a>
						</div>
					</div>	
					
					<div class="col-md-5">
						<div class="slider-text">
							<h3 class="title" id="txtNome">Obra</h3>
							<div class="short-info" >
								<p><font id="obr_categoria" style="font-size: 18px; "> </font></p>
							</div>
							
							<div class="contact-with" style="margin-bottom: 20px;">
								<h4 style="font-size: 16px; margin-top: 0;">Artista </h4>
								<p><strong id="usu_nome"></strong> </p>
								<p><strong id="usu_email"> </strong> </p>
								<p><strong id="usu_telefone">Data de Nascimento: </strong> </p>							
                            </div>	
                            <div class="short-info">
                                <div>
                                <h4>Descrição</h4>
                                    <p id="dspDescricao"></p>
                                </div> 
                            </div>			
						</div>
					</div>
                    				
				</div>				
			</div>
		</div>
	</section>
	
	<?php include 'includes/footer.php'; ?>


    <?php include 'includes/scripts.php'; ?>
	<?php include 'includes/verifica-menu.php'; ?>
	<script src="controller/obra.js"></script>
  </body>
  <script type="text/javascript">
    <?php if($_GET['id_obra'] != ""){ ?>
        obra.detalhar(<?php echo $_GET['id_obra']; ?>);
    <?php } ?>

	$("#btnPesquisar").on("click", function(){
		if($("#txtPesquisar").val() != ""){
			window.location.href = 'listar_artistas.php?getpesquisa='+$("#txtPesquisar").val();
		}
	});
  </script>

</html>