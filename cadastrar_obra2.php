<!DOCTYPE html>
<html lang="pt-BR">
	  
	<?php include 'includes/head.php'; ?>
	<link rel="stylesheet" href="css/dropzone.css">
  <body>
	
	<?php include 'includes/topo.php'; ?>

	<section id="main" class="clearfix ad-details-page">
		<div class="container">
		
			<div class="breadcrumb-section">
				<ol class="breadcrumb">
					<li><a href="index.php">Home</a></li>
					<li>Cadastro de obra</li>
				</ol>
				<h2 class="title">Passo II</h2>
			</div>

			<div class="adpost-details">
				<div class="row">	
					<div class="col-md-8">
						<form id="form1" enctype="multipart/form-data">
							<input type="hidden" name="cat_obra_id" id="cat_obra_id">
							<input type="hidden" name="usu_id" id="usu_id">
							<fieldset>
								<div class="section postdetails">
									<div class="form-group selected-product">
										<ul id="categoDetails" class="select-category list-inline">
								
										</ul>
										<a class="edit" href="javascript:;" id="editCatego"><i class="fa fa-pencil"></i>Editar</a>
									</div>
									<div class="row form-group add-title">
										<label class="col-sm-3 label-title">Título <span class="required">*</span></label>
										<div class="col-sm-9">
											<input type="text" required class="form-control" id="obr_titulo" name="obr_titulo">
										</div>
									</div>
									<div class="row form-group add-image">
										<label class="col-sm-3 label-title">Fotos </label>
										<div class="col-sm-9">
											<div id="my-dropzone" action="https://www.doocati.com.br/tcc/client/uploadCustomer.php?id=2" class="dropzone"></div>
										</div>
									</div>
									<div class="row form-group item-description">
										<label class="col-sm-3 label-title">Descrição<span class="required">*</span></label>
										<div class="col-sm-9">
											<textarea class="form-control" required id="obr_descricao" name="obr_descricao" rows="8"></textarea>		
										</div>
									</div>			
								</div>
								
								<div class="checkbox section agreement">
									<label for="send">
										<input type="checkbox" name="send" id="send">
										Lí e concordo com os <a href="#">Termos de uso</a> e a <a href="#">Política de privacidade</a> do site e desejo continuar com a operação.
									</label>
									<button type="button" id="btnCadastrar" class="btn btn-primary">Finalizar</button>
								</div>
								<select name="fotos" id="fotos" class="form-control" multiple style="display:none;"></select>
							</fieldset>
						</form>
					</div>
				

					<!-- quick-rules -->	
					<div class="col-md-4">
						<div class="section quick-rules">
							<h4>Rápidas</h4>
							<p class="lead">Todas as obras do <a href="#">Artes online</a> são grátis! Portanto, algumas regras devem ser seguidas:</p>

							<ul>
								<li>Crie sua obra na categoria correta.</li>
								<li>Cuidado para não criar obras repetidas,você será advertido.</li>
								<li>Não faça upload de imagens com qualquer propaganda.</li>
								<li>Não cadastre email, telefone ou qualquer informação falsa.</li>
								<li>Uma descrição com escrita limpa e fluente é sempre mais visualizada.</li>
								<li>Após cadastrar sua obra,ela precisa ser aprovada para ser publicada.</li>
							</ul>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</section>
	
	<?php include 'includes/footer.php'; ?>
	
   	<?php include 'includes/scripts.php'; ?>
   	<?php include 'includes/verifica-menu.php'; ?>
	<script src="js/dropzone.js"></script>
	   <script src="controller/obra.js"></script>
  </body>
  <script type="text/javascript">
  	$(document).ready(function() {
		
		$("#usu_id").val(getUsuarioLogado());

		Dropzone.autoDiscover = false;
		$("#my-dropzone").dropzone({
                addRemoveLinks: true,
				renameFilename: function (filename) {
					$('#mySelect').append($('<option>', {
						value: 1,
						text: 'My option'
					}));
					return 'img/'+filename;
				},
				
                init: function() {
					 this.on("success", function(file, Data) {
						var nomeImg = file.previewElement.querySelector("[data-dz-name]");
						var option = document.createElement('option');
							option.setAttribute("value",nomeImg.textContent);
							option.setAttribute("name","foto");
							document.querySelector("#fotos").appendChild(option);
					});
					this.on("removedfile", function(file) {
						var nomeImg = file.previewElement.querySelector("[data-dz-name]");
						$("#fotos option[value='"+nomeImg.textContent+"']").remove();
						console.log('tentou');
					});
                },
				
            });

	});
	</script>
   <script type="text/javascript">

   $("#btnCadastrar").on("click", function(){
	  $("#form1").validate({
			highlight: function (e, ec, vc) { 
				$(e).parents("div.form-group").addClass("has-error"); 
			}, 
			unhighlight: function (e, ec, vc) { 
				$(e).parents(".has-error").removeClass("has-error"); 
			}
		});
	   	if($("#form1").valid() == true){
	  		obra.inserir(getUsuarioLogado());
  		}
   });

   $('#ate_cep').on('blur',function(){
	   if($(this).val() != ""){
		   obra.buscarCepCorreios($(this).val());
	   }
   });
   $("#editCatego").on('click', function(){
	   Cookies.remove('chosenCat');
	   window.location.href = 'cadastrar_obra.php';
   });

   if(Cookies.get('chosenCat') !== undefined){
	   $("#categoDetails").append('<li class="active">'+
		' <a href="javascript:;">'+
			'<span class="select">' +
			'<img src="images/icon/'+Cookies.getJSON('chosenCat').img+'.png" alt="Images" class="img-responsive">'+
			'</span>' +
			Cookies.getJSON('chosenCat').nome +
			'</a>'+
		'</li>');
		$("#cat_obra_id").val(Cookies.getJSON('chosenCat').id);
   }
   else{
	   window.location.href = 'cadastrar_obra.php';
   }

  
  </script>

</html>